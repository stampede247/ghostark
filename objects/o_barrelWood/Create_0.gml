/// @desc Init Variables

self.possessionAmount = 0;
self.possessionDecayDelay = 0;
self.isFullyPossessed = false;
self.objectTime = 0;

self.isGrounded = true;
self.velocity_x = 0;
self.velocity_y = 0;
self.gravityAccel = 0.3;
self.feetOffset = 31;

self.inputLast_left = false;
self.inputLast_right = false;
self.inputLast_jump = false;

if (self.isMetal)
{
	sprite_index = s_barrelMetal;
}
if (self.isWater)
{
	sprite_index = s_barrelWater;
}

