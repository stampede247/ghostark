/// @desc Consume ghost players

if (other.isGhost and !self.isFullyPossessed and self.possessionAmount < 1.0)
{
	instance_destroy(other);
	self.possessionAmount += self.possessionSpeed;
	if (self.possessionAmount >= 1.0)
	{
		self.possessionAmount = 1.0;
		audio_play_sound(snd_possess, 0, false);
		self.isFullyPossessed = true;
	}
	self.possessionDecayDelay = 1000;
	audio_play_sound(snd_ghostConsumed, 0, false);
}