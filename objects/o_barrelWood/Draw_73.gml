/// @desc Draw possession bar

if (self.possessionAmount > 0)
{
	var barRec_x1 = bbox_left + (bbox_right - bbox_left)/2 - 20;
	var barRec_y1 = bbox_top - 10 - 5;
	var barRec_x2 = barRec_x1 + 40;
	var barRec_y2 = barRec_y1 + 5;
	draw_set_color(c_dkgray);
	draw_rectangle(barRec_x1, barRec_y1, barRec_x2, barRec_y2, false);
	draw_set_color(c_aqua);
	if (self.isFullyPossessed)
	{
		var oscillateAmount = sin(((self.objectTime mod 1000)/1000)*2*pi)/2 + 0.5;
		draw_set_color(merge_color(c_aqua, c_white, oscillateAmount));
	}
	var fill_x2 = barRec_x1+1 + (barRec_x2 - barRec_x1 - 2)*self.possessionAmount;
	if (fill_x2 > barRec_x2) { fill_x2 = barRec_x2; }
	if (fill_x2 < barRec_x1+1) { fill_x2 = barRec_x1+1; }
	draw_rectangle(barRec_x1+1, barRec_y1+1, fill_x2-1, barRec_y2-1, false);
}
