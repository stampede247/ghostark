/// @desc Exorsise slowly

var timeScale = ((delta_time/1000) / (1000/60));
if (self.possessionDecayDelay <= 0 or self.isFullyPossessed)
{
	if (self.possessionAmount > 0 and global.eventTime == 0)
	{
		if (self.isFullyPossessed) { self.possessionAmount -= 0.004*timeScale; }
		else { self.possessionAmount -= 0.005*timeScale; }
		if (self.possessionAmount <= 0)
		{
			self.possessionAmount = 0;
			if (self.isFullyPossessed)
			{
				audio_play_sound(snd_exorcise, 0, false);
				self.isFullyPossessed = false;
			}
		}
	}
}
else if (global.eventTime == 0)
{
	self.possessionDecayDelay -= delta_time/1000;
	if (self.possessionDecayDelay <= 0) { self.possessionDecayDelay = 0; }
}

//Update movement
if (global.eventTime == 0)
{
	var input_left = keyboard_check(vk_left);
	var input_right = keyboard_check(vk_right);
	var input_jump = keyboard_check(ord("Z"));
	
	var maxWalkSpeed = 0;
	var maxStepHeight = 0;
	var jumpSpeed = 0;
	if (self.isFullyPossessed)
	{
		maxWalkSpeed = 3;
		maxStepHeight = 5;
		jumpSpeed = 4;
	}
	else if (self.possessionAmount >= 0.75)
	{
		maxWalkSpeed = 2;
	}
	else if (self.possessionAmount >= 0.4)
	{
		maxWalkSpeed = 1;
	}
	
	if (maxWalkSpeed > 0 or velocity_x > 0 or velocity_y > 0 or !isGrounded)
	{
		velocity_x = (input_right - input_left) * maxWalkSpeed;
		velocity_y += gravityAccel;

		var moveVars = array_create(4);
		moveVars[0] = x;
		moveVars[1] = y;
		moveVars[2] = velocity_x;
		moveVars[3] = velocity_y;
		move_with_collision(moveVars, maxStepHeight, isGrounded, true);
		x          = moveVars[0];
		y          = moveVars[1];
		velocity_x = moveVars[2];
		velocity_y = moveVars[3];

		isGrounded = is_grounded(velocity_y >= 0);

		if (input_jump and !inputLast_jump and jumpSpeed > 0)
		{
			velocity_y = -jumpSpeed;
			isGrounded = false;
		}
	}
	
	inputLast_left = input_left;
	inputLast_right = input_right;
	inputLast_jump = input_jump;
}

self.objectTime += delta_time/1000;
