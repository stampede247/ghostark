/// @desc Update eventTime

global.eventTime -= delta_time/1000;
if (global.eventTime <= 0) { global.eventTime = 0; }
global.fallEventTime -= delta_time/1000;
if (global.fallEventTime <= 0) { global.fallEventTime = 0; }

if (keyboard_check_pressed(ord("R")))
{
	room_restart();
	global.ghostCountLeftStart  = resetGhostCountLeftStart;
	global.ghostCountLeftStop   = resetGhostCountLeftStop;
	global.ghostCountRightStart = resetGhostCountRightStart;
	global.ghostCountRightStop  = resetGhostCountRightStop;
	global.ghostCountJumpStart  = resetGhostCountJumpStart;
	global.ghostCountJumpStop   = resetGhostCountJumpStop;
}
