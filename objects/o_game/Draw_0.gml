/// @desc Draw nothing

if (room_get_name(room) == "r_end")
{
	textPos_x = room_width/2;
	textPos_y = room_height/6;
	draw_set_color(c_black);
	draw_set_font(endFont);
	draw_set_halign(fa_center);
	draw_text(textPos_x, textPos_y, "Thank you for playing!");
	textPos_y += 50;
	var timeStr = string(floor(global.playTime/1000/60/60)) + "h " + string(floor((global.playTime/1000/60) mod 60)) + "m " + string(floor((global.playTime/1000) mod (60))) + "s";
	draw_text(textPos_x, textPos_y, timeStr);
	textPos_y += 50;
	draw_text(textPos_x, textPos_y, string(global.ghostCountRightStop) + "/4 green " + string(global.ghostCountLeftStop) + "/4 purple " + string(global.ghostCountJumpStart) + "/4 red");
}
