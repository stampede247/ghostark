/// @desc Handle Music

if (!audio_is_playing(music_mainLoop) and room_get_name(room) != "r_end")
{
	audio_play_sound(music_mainLoop, 1, true);
}
if (room_get_name(room) == "r_end" and !audio_is_playing(snd_applause) and !audio_is_playing(snd_applause) and !audio_is_playing(music_ending))
{
	audio_play_sound(music_ending, 1, true);
}

if (self.startEventTime > 0)
{
	global.fallEventTime = self.startEventTime;
	self.startEventTime = 0;
}

if (room_get_name(room) != "r_end")
{
	global.playTime += delta_time/1000;
}