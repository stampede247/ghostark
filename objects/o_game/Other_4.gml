/// @desc Save reset powerups

resetGhostCountLeftStart  = global.ghostCountLeftStart;
resetGhostCountLeftStop   = global.ghostCountLeftStop;
resetGhostCountRightStart = global.ghostCountRightStart;
resetGhostCountRightStop  = global.ghostCountRightStop;
resetGhostCountJumpStart  = global.ghostCountJumpStart;
resetGhostCountJumpStop   = global.ghostCountJumpStop;

if (room_get_name(room) == "r_end")
{
	audio_stop_sound(music_mainLoop);
	audio_play_sound(snd_applause, 0, false);
}