{
    "id": "ba38eb83-29af-40b0-ba2d-77b107666416",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_game",
    "eventList": [
        {
            "id": "aa4cd193-c5cc-4926-aec0-9899b1a18c9c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "ba38eb83-29af-40b0-ba2d-77b107666416"
        },
        {
            "id": "ffdc04d6-42c8-4bba-86ba-93dbd461a1a2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "ba38eb83-29af-40b0-ba2d-77b107666416"
        },
        {
            "id": "e9aaf453-0c2e-4bc2-8070-b4d0a698f095",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 7,
            "m_owner": "ba38eb83-29af-40b0-ba2d-77b107666416"
        },
        {
            "id": "43085743-fb85-4e89-915c-b7aa92a6abb2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ba38eb83-29af-40b0-ba2d-77b107666416"
        },
        {
            "id": "13efb074-70e1-4c9d-941e-c0e5943ab729",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "ba38eb83-29af-40b0-ba2d-77b107666416"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "cb6e8b53-7e14-4143-af53-0de72d719ef8",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "startEventTime",
            "varType": 0
        }
    ],
    "solid": false,
    "spriteId": "426d64bf-c92a-4950-9bd8-f93e5d128566",
    "visible": true
}