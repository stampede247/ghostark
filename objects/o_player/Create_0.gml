/// @desc Intialize object variables

velocity_x = 0;
velocity_y = 0;
isGrounded = false;
groundedTimer = 0;
lastGroundedTime = 0;
facingRight = true;
objectTime = 0;
moveTime = 0;

gravityAccel = 0.3;
maxWalkSpeed = 4;
groundAccel = 1;
groundDecel = 0.5;
airAccel = 0.1;
airDecel = 0.01;
jumpSpeed = 7.3;
viewLag = 3;
feetOffset = 12;

inputLast_right = false;
inputLast_left = false;
inputLast_down = false;
inputLast_up = false;
inputLast_jump = false;

isGhost = false;
ghostColor = c_white;
ghostInputChangeTime = 0;
ghostFadeTime = 0;
ghostDeathTime = 0;
ghostInputStart_left = false;
ghostInputStart_right = false;
ghostInputStart_down = false;
ghostInputStart_up = false;
ghostInputStart_jump = false;
ghostInputEnd_left = false;
ghostInputEnd_right = false;
ghostInputEnd_down = false;
ghostInputEnd_up = false;
ghostInputEnd_jump = false;
