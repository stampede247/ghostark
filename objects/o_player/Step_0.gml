/// @desc Update movement, collision, and camera if not ghost

var input_left = false;
var input_right = false;
var input_down = false;
var input_up = false;
var input_jump = false;
if (isGhost)
{
	if (objectTime >= ghostInputChangeTime)
	{
		input_left = ghostInputEnd_left;
		input_right = ghostInputEnd_right;
		input_down = ghostInputEnd_down;
		input_up = ghostInputEnd_up;
		input_jump = ghostInputEnd_jump;
	}
	else
	{
		input_left = ghostInputStart_left;
		input_right = ghostInputStart_right;
		input_down = ghostInputStart_down;
		input_up = ghostInputStart_up;
		input_jump = ghostInputStart_jump;
	}
}
else
{
	input_left = keyboard_check(vk_left);
	input_right = keyboard_check(vk_right);
	input_down = keyboard_check(vk_down);
	input_up = keyboard_check(vk_up);
	input_jump = keyboard_check(ord("Z"));
}

if (global.eventTime > 0 and !isGhost)
{
	input_left = false;
	input_right = false;
	input_down = false;
	input_up = false;
	input_jump = false;
}
if (global.fallEventTime > 0 and !isGhost)
{
	input_left = false;
	input_right = false;
}
if (input_down)
{
	input_left = false;
	input_right = false;
	input_jump = false;
	sprite_index = s_ghostBoyBodyCrouch;
}
else
{
	sprite_index = s_ghostBoyBody;
}

var currentlyOnPlatform = is_on_platform();
var blueGhostCount = 0;
var greenGhostCount = 0;
var purpleGhostCount = 0;
var redGhostCount = 0;
if (!isGhost and global.eventTime == 0)
{
	if (!input_left  and !input_right and inputLast_left and isGrounded)  { purpleGhostCount += global.ghostCountLeftStop; }
	if (!input_right and !input_left  and inputLast_right and isGrounded) { greenGhostCount += global.ghostCountRightStop; }
	//if (!input_down  and inputLast_down)
	//{
	//	if (currentlyOnPlatform) { inputChanged = true; }
	//}
	// if (!input_up and inputLast_up)       { inputChanged = true; }
	if (!input_jump  and  inputLast_jump)  { redGhostCount += global.ghostCountJumpStop; }
	if (input_left   and !input_right and !inputLast_left and isGrounded)  { purpleGhostCount += global.ghostCountLeftStart; }
	if (input_right  and !input_left  and !inputLast_right and isGrounded) { blueGhostCount += global.ghostCountRightStart; }
	// if (input_down and !inputLast_down)   { inputChanged = true; }
	// if (input_up and !inputLast_up)       { inputChanged = true; }
	if (input_jump  and !inputLast_jump)  { redGhostCount += global.ghostCountJumpStart; }
}
var totalGhostCount = blueGhostCount + greenGhostCount + redGhostCount + purpleGhostCount;

if (totalGhostCount > 0)
{
	for (var i = 0; i < totalGhostCount; i++)
	{
		var newGhost = instance_create_layer(x, y, "Entities", o_player);
		newGhost.isGhost = true;
		var timingOffset = 150*(i+1);
		newGhost.ghostInputChangeTime = timingOffset;
		newGhost.ghostFadeTime = 1500 + timingOffset;
		newGhost.ghostDeathTime = 2000 + timingOffset;
		newGhost.ghostInputStart_left = inputLast_left;
		newGhost.ghostInputStart_right = inputLast_right;
		newGhost.ghostInputStart_jump = inputLast_jump;
		newGhost.ghostInputEnd_left = input_left;
		newGhost.ghostInputEnd_right = input_right;
		newGhost.ghostInputEnd_jump = input_jump;
		newGhost.ghostColor = c_blue;
		if (i >= blueGhostCount) { newGhost.ghostColor = c_green; }
		if (i >= blueGhostCount + greenGhostCount) { newGhost.ghostColor = c_red; }
		if (i >= blueGhostCount + greenGhostCount + redGhostCount) { newGhost.ghostColor = c_fuchsia; }
	}
}

if (global.eventTime == 0)
{
	if (input_right) { facingRight = true; }
	else if (input_left) { facingRight = false; }
}

if (global.eventTime == 0)
{
	velocity_x = (input_right - input_left) * maxWalkSpeed;
	velocity_y += gravityAccel;

	var moveVars = array_create(4);
	moveVars[0] = x;
	moveVars[1] = y;
	moveVars[2] = velocity_x;
	moveVars[3] = velocity_y;
	move_with_collision(moveVars, 10, isGrounded, true);
	x          = moveVars[0];
	y          = moveVars[1];
	velocity_x = moveVars[2];
	velocity_y = moveVars[3];
	
	var currentlyGrounded = is_grounded(velocity_y >= 0);
	if (currentlyGrounded)
	{
		groundedTimer = 200;
		isGrounded = true;
	}
	else
	{
		groundedTimer -= delta_time/1000;
		if (groundedTimer <= 0) { isGrounded = false; }
	}

	//Handle Jump
	if (input_jump and !inputLast_jump and isGrounded)
	{
		velocity_y = -jumpSpeed;
		isGrounded = false;
		groundedTimer = 0;
	}
	
	if (isGrounded and abs(velocity_x) > 0 and !isGhost)
	{
		moveTime += delta_time/500;
		if (moveTime >= 500)
		{
			moveTime -= 500;
			if (place_meeting(x, y, o_mushroomArea))
			{
				audio_play_sound(snd_footstepMushroom1, 0, false);
			}
			else if (place_meeting(x, y, o_woodArea))
			{
				audio_play_sound(snd_footstepWood1, 0, false);
			}
			else
			{
				audio_play_sound(snd_footstep1, 0, false);
			}
		}
	}
	else { moveTime = 250; }
}

objectTime += delta_time/1000;

if (isGhost)
{
	if (objectTime > 3000) { instance_destroy(self); }
}
else
{
	var viewTarget_x = x - camera_get_view_width(camera)/2;
	var viewTarget_y = y - camera_get_view_height(camera)*(3/8);
	var viewPos_x = camera_get_view_x(camera);
	var viewPos_y = camera_get_view_y(camera);
	var newViewPos_x = viewPos_x + (viewTarget_x - viewPos_x) / viewLag;
	var newViewPos_y = viewPos_y + (viewTarget_y - viewPos_y) / viewLag;
	camera_set_view_pos(camera, newViewPos_x, newViewPos_y);
}

inputLast_left = input_left;
inputLast_right = input_right;
inputLast_down = input_down;
inputLast_up = input_up;
inputLast_jump = input_jump;