/// @desc Draw all body parts (and debug text)

var animationOneSecAngle = ((objectTime mod 1000)/1000)*2*pi;
var animationThirdSecAngle = ((objectTime mod 300)/300)*2*pi;
var facingSign = 1;
var drawColor = c_white;
var drawAlpha = 1;
var drawFrame = 0;
if (isGhost)
{
	drawColor = ghostColor;
	drawAlpha = 0.5;
	if (objectTime >= ghostFadeTime)
	{
		drawAlpha *= (1 - (objectTime - ghostFadeTime) / (ghostDeathTime - ghostFadeTime));
	}
}
if (!facingRight) { drawFrame = 1; facingSign = -1; }

//Draw Right Foot
var rightFootPos_x = x + 4*facingSign;
var rightFootPos_y = y + 11;
if (velocity_x > 0)
{
	rightFootPos_x += cos(animationThirdSecAngle + pi) * 2;
	rightFootPos_y += sin(animationThirdSecAngle + pi) * 2;
}
else if (velocity_x < 0)
{
	rightFootPos_x += cos(animationThirdSecAngle + pi) * 2;
	rightFootPos_y -= sin(animationThirdSecAngle + pi) * 2;
}
draw_sprite_ext(s_ghostBoyFootRight, 0, rightFootPos_x, rightFootPos_y, facingSign, 1, 0, drawColor, drawAlpha);

//Draw body
image_blend = drawColor;
image_alpha = drawAlpha;
image_index = drawFrame;
draw_self();

//Draw head
var headPos_x = x + cos(animationOneSecAngle)*facingSign;
var headPos_y = y + sin(animationOneSecAngle);
if (sprite_index == s_ghostBoyBodyCrouch) { headPos_y -= 6; }
else { headPos_y -= 12; }
draw_sprite_ext(s_ghostBoyHead, drawFrame, headPos_x, headPos_y, 1, 1, 0, drawColor, drawAlpha);

//Draw left foot
var leftFootPos_x = x - 4*facingSign;
var leftFootPos_y = y + 11;
if (velocity_x > 0)
{
	leftFootPos_x += cos(animationThirdSecAngle) * 2;
	leftFootPos_y += sin(animationThirdSecAngle) * 2;
}
else if (velocity_x < 0)
{
	leftFootPos_x += cos(animationThirdSecAngle) * 2;
	leftFootPos_y -= sin(animationThirdSecAngle) * 2;
}
draw_sprite_ext(s_ghostBoyFootLeft, 0, leftFootPos_x, leftFootPos_y, facingSign, 1, 0, drawColor, drawAlpha);

if (!isGhost)
{
	draw_set_color(c_white);
	draw_set_font(debugFont);
	draw_set_halign(fa_center);
	// draw_text(x, self.bbox_top - 20, "h: " + string(velocity_x) + " v: " + string(velocity_y));
	// if (foundGroundToWalkOn)
	// {
	// 	draw_text(x, self.bbox_top - 20, "grounded");
	// }
	// else
	// {
	// 	draw_text(x, self.bbox_top - 20, "air");
	// }
}


