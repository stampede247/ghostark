/// @desc Create a view if not a ghost

if (!isGhost)
{
	camera = camera_create_view(0, 0, 512, 384, 0, -1, -1, -1, 32, 32);
	view_set_camera(0, camera);
}
