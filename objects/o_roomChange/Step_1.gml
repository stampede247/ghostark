/// @desc Update wasColliding

self.isColliding = false;

if (self.multiWarp)
{
	if (keyboard_check_pressed(ord("1")))
	{
		self.targetRoomName = "r_area1";
	}
	if (keyboard_check_pressed(ord("2")))
	{
		self.targetRoomName = "r_area2";
	}
	if (keyboard_check_pressed(ord("3")))
	{
		self.targetRoomName = "r_area3";
	}
	if (keyboard_check_pressed(ord("4")))
	{
		self.targetRoomName = "r_area4";
	}
	if (keyboard_check_pressed(ord("5")))
	{
		self.targetRoomName = "r_end";
	}
}
