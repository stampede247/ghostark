/// @desc Go to room

if (!other.isGhost)
{
	if (!self.wasColliding)
	{
		if (self.targetRoomName == "r_end")
		{
			audio_play_sound(snd_winGame, 0, false);
		}
		var roomIndex = asset_get_index(self.targetRoomName);
		if (roomIndex > 0)
		{
			room_goto(roomIndex);
		}
		else
		{
			show_debug_message("Couldn't find room \"" + self.targetRoomName + "\"");
		}
	}
	self.isColliding = true;
}
