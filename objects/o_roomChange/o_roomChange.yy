{
    "id": "3a26e942-f80b-4312-9b06-894a8192b0f4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_roomChange",
    "eventList": [
        {
            "id": "a77a301d-f773-42fb-9be1-52f39673f18e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "3a26e942-f80b-4312-9b06-894a8192b0f4"
        },
        {
            "id": "8081e8fa-6329-4778-bde1-b43ed8fa7345",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "445d2d28-80da-4eab-8488-6bdf29888618",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "3a26e942-f80b-4312-9b06-894a8192b0f4"
        },
        {
            "id": "70202a8d-7c8f-48d2-9c88-7a7115fcbbef",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3a26e942-f80b-4312-9b06-894a8192b0f4"
        },
        {
            "id": "c611777c-d35e-4409-aa7e-ff78dafe3bfb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "3a26e942-f80b-4312-9b06-894a8192b0f4"
        },
        {
            "id": "10bcd53b-fbdc-4033-a80f-70d3f7c7a244",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "3a26e942-f80b-4312-9b06-894a8192b0f4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "3570181f-1e6b-46a1-b650-528e866e2e03",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "r_area1",
            "varName": "targetRoomName",
            "varType": 2
        },
        {
            "id": "80f2b9bb-53ae-467c-9218-0d4039492e43",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "multiWarp",
            "varType": 3
        }
    ],
    "solid": false,
    "spriteId": "148fd230-fe6d-40f7-a4f3-59ccb5324bfe",
    "visible": true
}