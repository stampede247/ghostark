{
    "id": "ba2284bf-3403-420f-a651-62a82e1e969b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_woodArea",
    "eventList": [
        {
            "id": "8c33058b-0d1e-40be-9913-54ef334f0ad9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "ba2284bf-3403-420f-a651-62a82e1e969b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2e52e7c3-18a4-443f-b0a8-bcf42cc874c1",
    "visible": true
}