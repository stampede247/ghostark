/// @desc Update animation

if (self.animTime > 0)
{
	var newAnimTime = self.animTime - delta_time/1000;
	if (newAnimTime < 0) { newAnimTime = 0; }
	if (self.animTime >= 2500 && newAnimTime < 2500)
	{
		audio_play_sound(snd_haunt1, 0, false);
	}
	if (self.animTime >= 2000 && newAnimTime < 2000)
	{
		self.sprite_index = s_skullActive;
		self.image_speed = 1;
	}
	if (newAnimTime > 2000)
	{
		self.image_xscale = 1 + (1 - (newAnimTime - 2000) / 1000)*3;
		self.image_yscale = self.image_xscale;
	}
	else if (newAnimTime > 1000)
	{
		self.image_xscale = 4;
		self.image_yscale = self.image_xscale;
	}
	else
	{
		self.image_xscale = 1 + (newAnimTime / 1000)*3;
		self.image_yscale = self.image_xscale;
	}
	self.animTime = newAnimTime;
}

if (self.shortAnimTime > 0)
{
	var newAnimTime = self.shortAnimTime - delta_time/1000;
	if (newAnimTime < 0) { newAnimTime = 0; }
	if (self.shortAnimTime >= 900 && newAnimTime < 900)
	{
		audio_play_sound(snd_haunt1, 0, false);
	}
	if (self.shortAnimTime >= 600 && newAnimTime < 600)
	{
		self.sprite_index = s_skullActive;
		self.image_speed = 1;
	}
	if (newAnimTime > 600)
	{
		self.image_xscale = 1 + (1 - (newAnimTime - 600) / 400)*3;
		self.image_yscale = self.image_xscale;
	}
	else if (newAnimTime > 400)
	{
		self.image_xscale = 4;
		self.image_yscale = self.image_xscale;
	}
	else
	{
		self.image_xscale = 1 + (newAnimTime / 400)*3;
		self.image_yscale = self.image_xscale;
	}
	self.shortAnimTime = newAnimTime;
}
