/// @desc Light up and change player's ghost count

if (!activated and !other.isGhost)
{
	var longEvent = false;
	if (self.addRightStart)
	{
		if (global.ghostCountRightStart == 0) { longEvent = true; }
		global.ghostCountRightStart += 1;
	}
	if (self.addRightStop)
	{
		if (global.ghostCountRightStop == 0) { longEvent = true; }
		global.ghostCountRightStop += 1;
	}
	if (self.addLeftStart)
	{
		if (global.ghostCountLeftStart == 0) { longEvent = true; }
		global.ghostCountLeftStart += 1;
	}
	if (self.addLeftStop)
	{
		if (global.ghostCountLeftStop == 0) { longEvent = true; }
		global.ghostCountLeftStop += 1;
	}
	if (self.addJumpStart)
	{
		if (global.ghostCountJumpStart == 0) { longEvent = true; }
		global.ghostCountJumpStart += 1;
	}
	if (self.addJumpStop)
	{
		if (global.ghostCountJumpStop == 0) { longEvent = true; }
		global.ghostCountJumpStop += 1;
	}
	if (longEvent)
	{
	    global.eventTime = 3000; //3 second event
	    self.animTime = 3000;
	}
	else
	{
	    self.shortAnimTime = 1000;
	}
    activated = true;
    audio_play_sound(snd_discover, 0, false);
}
