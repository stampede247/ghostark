{
    "id": "c1b7d3d4-7523-487a-a544-1ecb19521815",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_collisionSpiritDoor",
    "eventList": [
        {
            "id": "d6194918-9754-4be8-8fc8-69873c48d66d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "c1b7d3d4-7523-487a-a544-1ecb19521815"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "28abb8a1-4c78-4524-96c1-874d7bdbe69f",
    "visible": true
}