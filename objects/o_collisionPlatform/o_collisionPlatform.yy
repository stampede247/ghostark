{
    "id": "d66c8703-c4e5-4f9c-b7e0-113b335f4a7c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_collisionPlatform",
    "eventList": [
        {
            "id": "a91f3b74-66d5-4925-9892-073e1023fa03",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "d66c8703-c4e5-4f9c-b7e0-113b335f4a7c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "311304fd-a6ae-4641-ac61-9647211e0dc5",
    "visible": true
}