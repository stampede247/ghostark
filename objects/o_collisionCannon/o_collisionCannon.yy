{
    "id": "75c988ea-d4d9-458e-b76a-07acbc0de8e3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_collisionCannon",
    "eventList": [
        {
            "id": "93f43a2e-623c-4677-8621-569fc44fbe8c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "445d2d28-80da-4eab-8488-6bdf29888618",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "75c988ea-d4d9-458e-b76a-07acbc0de8e3"
        },
        {
            "id": "b8462838-155d-448e-bb1d-e0f134f331a2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "75c988ea-d4d9-458e-b76a-07acbc0de8e3"
        },
        {
            "id": "b31ce992-f2e8-49cc-9b8c-76cce9600840",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "75c988ea-d4d9-458e-b76a-07acbc0de8e3"
        },
        {
            "id": "fae8bed0-da63-4534-8e02-019f623dedc0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "75c988ea-d4d9-458e-b76a-07acbc0de8e3"
        },
        {
            "id": "e5bfdfc2-2cd7-4313-a22f-a2f93032f352",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "75c988ea-d4d9-458e-b76a-07acbc0de8e3"
        },
        {
            "id": "08f7a73e-2c3b-41a2-943c-ff659a8a798b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "75c988ea-d4d9-458e-b76a-07acbc0de8e3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "637ddca9-3fb7-4a0b-a6a6-59b4ea28d362",
    "visible": true
}