/// @desc Fire confetti

if (self.reloadTime == 0 and !self.wasTouching or other.inputLast_down)
{
	audio_play_sound(snd_cannonFire, 0, false);
	for (var i = 0; i < 100; i++)
	{
		var newPart = instance_create_layer(x, y, "Background", o_confetti);
		var fireDir = self.image_angle+pi/2 + random_range(-0.3, 0.3);
		var partSpeed = random_range(1, 14);
		newPart.velocity_x = cos(fireDir)*partSpeed;
		newPart.velocity_y = sin(fireDir)*partSpeed;
	}
	self.reloadTime = 3000;
}
self.isTouching = true;
