{
    "id": "3e98c148-09a5-4b9e-9812-6a9dd5167b28",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_collisionSlope1",
    "eventList": [
        {
            "id": "2c03fd2b-5c24-45bd-96a8-7375cebbc3b2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "3e98c148-09a5-4b9e-9812-6a9dd5167b28"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2125e333-45f7-4acb-b67d-1d87e9e3f29f",
    "visible": true
}