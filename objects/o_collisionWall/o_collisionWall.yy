{
    "id": "97893052-b1bb-4446-b5f0-d939ad89e0b5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_collisionWall",
    "eventList": [
        {
            "id": "fb0adf96-fb1e-4411-a33a-53ab97cb6c8c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "97893052-b1bb-4446-b5f0-d939ad89e0b5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "273286fd-9d5f-428c-93ec-a9b6429afc4b",
    "visible": true
}