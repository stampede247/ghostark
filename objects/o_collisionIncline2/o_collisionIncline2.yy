{
    "id": "6b429f55-7b74-403f-94e4-755b5560313f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_collisionIncline2",
    "eventList": [
        {
            "id": "aa820426-d8dd-4464-9194-b58888db3ac5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "6b429f55-7b74-403f-94e4-755b5560313f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2c97acff-7c2f-49ab-8d30-f0192d94e720",
    "visible": true
}