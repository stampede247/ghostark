{
    "id": "043fed92-0198-41b8-8bb4-98bf4d02c338",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_collisionIncline1",
    "eventList": [
        {
            "id": "b5c3489f-b8ff-4380-bfba-9d7561c8617b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "043fed92-0198-41b8-8bb4-98bf4d02c338"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "5231fad4-c049-4a31-850b-84e425dc9ef6",
    "visible": true
}