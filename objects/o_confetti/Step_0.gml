/// @desc Update trajectory

velocity_x *= 0.98;
velocity_y *= 0.98;
velocity_y += 0.1;

x += velocity_x;
y += velocity_y;

if (y > room_height-20) { instance_destroy(self); }
