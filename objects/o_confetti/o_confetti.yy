{
    "id": "ecd79f9d-2915-4793-952c-2847f3d75b4f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_confetti",
    "eventList": [
        {
            "id": "cb570dff-11de-4469-b624-44ca2598ba47",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ecd79f9d-2915-4793-952c-2847f3d75b4f"
        },
        {
            "id": "54f1f61a-4bc9-4bcd-851f-7c432403642b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ecd79f9d-2915-4793-952c-2847f3d75b4f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6a669084-0966-4b53-add2-c92f316181e1",
    "visible": true
}