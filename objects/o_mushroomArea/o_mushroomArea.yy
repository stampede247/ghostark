{
    "id": "b1eb9ba1-785b-4bbb-a44c-885f1f1d0e00",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_mushroomArea",
    "eventList": [
        {
            "id": "9a9ed8f8-3708-4cb1-8ce2-7194f9d0775a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "b1eb9ba1-785b-4bbb-a44c-885f1f1d0e00"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8ab56208-ed10-4ea8-80f4-d9fda0f083de",
    "visible": true
}