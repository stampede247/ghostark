{
    "id": "6ae8254a-aadb-491d-a3ae-d2a14e3a2aa9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_partDot",
    "eventList": [
        {
            "id": "492e7c47-18fb-4c72-a6ff-e7e245f33a12",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "6ae8254a-aadb-491d-a3ae-d2a14e3a2aa9"
        },
        {
            "id": "6bd05e86-09ce-420f-835f-290177478ebf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6ae8254a-aadb-491d-a3ae-d2a14e3a2aa9"
        },
        {
            "id": "4fed0a67-6d68-49e2-8890-e3865ec5d30b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6ae8254a-aadb-491d-a3ae-d2a14e3a2aa9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "4bc1c60a-a64e-4572-8414-da65ca509236",
    "visible": true
}