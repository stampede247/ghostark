/// @desc Draw with offset

var timeAngle = ((objectTime mod 3000)/3000)*2*pi;
var offset_x = cos(timeAngle)*4;
var offset_y = sin(timeAngle)*4;
x += offset_x;
y += offset_y;

draw_self();

x -= offset_x;
y -= offset_y;
