{
    "id": "4107f442-0db2-498c-87d4-bc7d0588a748",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_boat1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 33,
    "bbox_left": 4,
    "bbox_right": 31,
    "bbox_top": 2,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "66a0b349-2b12-4e3f-a9a5-5fa15d7b30c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4107f442-0db2-498c-87d4-bc7d0588a748",
            "compositeImage": {
                "id": "c1ff31e0-7a73-4996-901f-b8d69376485a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66a0b349-2b12-4e3f-a9a5-5fa15d7b30c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3678bc68-831d-48c0-9790-563d867b9c4e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66a0b349-2b12-4e3f-a9a5-5fa15d7b30c9",
                    "LayerId": "b5390eee-f9d6-43ea-9a69-7c826d566333"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 88,
    "layers": [
        {
            "id": "b5390eee-f9d6-43ea-9a69-7c826d566333",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4107f442-0db2-498c-87d4-bc7d0588a748",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 113,
    "xorig": 12,
    "yorig": 3
}