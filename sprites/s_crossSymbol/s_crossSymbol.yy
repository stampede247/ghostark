{
    "id": "dcb4a6d4-a386-4bbc-a4d5-5ee4cc72e153",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_crossSymbol",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 18,
    "bbox_left": 0,
    "bbox_right": 18,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ae8b9c96-6046-4dcc-b27f-104ed3c22392",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dcb4a6d4-a386-4bbc-a4d5-5ee4cc72e153",
            "compositeImage": {
                "id": "5487075f-a1c2-44ae-8955-382724b4e0ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae8b9c96-6046-4dcc-b27f-104ed3c22392",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3111f28b-17c3-4b34-a7e6-b0ffbdace9f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae8b9c96-6046-4dcc-b27f-104ed3c22392",
                    "LayerId": "9e08155c-1338-49bc-bb60-fbd64b841412"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 19,
    "layers": [
        {
            "id": "9e08155c-1338-49bc-bb60-fbd64b841412",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dcb4a6d4-a386-4bbc-a4d5-5ee4cc72e153",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 19,
    "xorig": 0,
    "yorig": 0
}