{
    "id": "4cf0087a-9ae1-4936-8ab2-511aa1e84ac0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_ghostBoyFootRight",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 6,
    "bbox_left": 1,
    "bbox_right": 10,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "951e218b-8571-46e3-a953-31ae2c203311",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cf0087a-9ae1-4936-8ab2-511aa1e84ac0",
            "compositeImage": {
                "id": "183b4be7-51ef-4600-9364-5a8c8a956340",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "951e218b-8571-46e3-a953-31ae2c203311",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89549840-d14c-4d12-934c-ae221eb2d9a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "951e218b-8571-46e3-a953-31ae2c203311",
                    "LayerId": "93c2ea35-337b-4ded-a3a1-74229f016dd3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "93c2ea35-337b-4ded-a3a1-74229f016dd3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4cf0087a-9ae1-4936-8ab2-511aa1e84ac0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 12,
    "xorig": 6,
    "yorig": 5
}