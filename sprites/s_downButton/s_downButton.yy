{
    "id": "d154e487-31ac-4b27-8b81-ae0781e6ceea",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_downButton",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 0,
    "bbox_right": 11,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "19f56e15-b958-4e4a-9d2c-a25c2dfc7b8c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d154e487-31ac-4b27-8b81-ae0781e6ceea",
            "compositeImage": {
                "id": "99786b7e-5f8c-4203-9478-1aa2f3ea83aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19f56e15-b958-4e4a-9d2c-a25c2dfc7b8c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2dc5e4ea-1481-40dd-ad83-b9e46260f727",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19f56e15-b958-4e4a-9d2c-a25c2dfc7b8c",
                    "LayerId": "474fd2a0-1645-412b-b3a8-7d1ef3636eac"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 12,
    "layers": [
        {
            "id": "474fd2a0-1645-412b-b3a8-7d1ef3636eac",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d154e487-31ac-4b27-8b81-ae0781e6ceea",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 12,
    "xorig": 0,
    "yorig": 0
}