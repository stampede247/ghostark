{
    "id": "aca3c40e-025d-4162-80df-3aee1c2191a8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_spiritDoor",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 89,
    "bbox_left": 0,
    "bbox_right": 16,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c22942fc-dec6-427a-a8e1-a55fdb6e3d6e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aca3c40e-025d-4162-80df-3aee1c2191a8",
            "compositeImage": {
                "id": "09eb65c5-31a6-414b-933e-b46c1ba71b3a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c22942fc-dec6-427a-a8e1-a55fdb6e3d6e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "539b77bd-849b-40f4-a7ed-f4d77fc5fbb1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c22942fc-dec6-427a-a8e1-a55fdb6e3d6e",
                    "LayerId": "45f2f069-1ed1-46ca-b1ef-bd227b27cdaa"
                }
            ]
        },
        {
            "id": "8f135e13-d0dc-427f-bc5b-e4b9d387797e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aca3c40e-025d-4162-80df-3aee1c2191a8",
            "compositeImage": {
                "id": "b0b49669-b003-4e62-b08b-4a1df3daa2c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f135e13-d0dc-427f-bc5b-e4b9d387797e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86912c2a-6a50-41c8-800d-cc6569cdda40",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f135e13-d0dc-427f-bc5b-e4b9d387797e",
                    "LayerId": "45f2f069-1ed1-46ca-b1ef-bd227b27cdaa"
                }
            ]
        },
        {
            "id": "cd9ec021-c582-4938-8ef7-ade0e2b146ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aca3c40e-025d-4162-80df-3aee1c2191a8",
            "compositeImage": {
                "id": "a6634f01-a4ce-4315-821a-b2b98fc6e3d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd9ec021-c582-4938-8ef7-ade0e2b146ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb7b62bc-570f-48c5-8e6c-841761370c7d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd9ec021-c582-4938-8ef7-ade0e2b146ba",
                    "LayerId": "45f2f069-1ed1-46ca-b1ef-bd227b27cdaa"
                }
            ]
        },
        {
            "id": "4c9c8e62-bbeb-490c-b6fa-84223fa41d8f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aca3c40e-025d-4162-80df-3aee1c2191a8",
            "compositeImage": {
                "id": "0f486eb6-2657-4b79-a981-167dd0881b1b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c9c8e62-bbeb-490c-b6fa-84223fa41d8f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff541bfd-45cb-4a3e-9b8f-7dffae802885",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c9c8e62-bbeb-490c-b6fa-84223fa41d8f",
                    "LayerId": "45f2f069-1ed1-46ca-b1ef-bd227b27cdaa"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 90,
    "layers": [
        {
            "id": "45f2f069-1ed1-46ca-b1ef-bd227b27cdaa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "aca3c40e-025d-4162-80df-3aee1c2191a8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 17,
    "xorig": 0,
    "yorig": 0
}