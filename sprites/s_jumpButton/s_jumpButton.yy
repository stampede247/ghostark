{
    "id": "8970979e-ca6e-4d77-8f3a-438d0fb050fc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_jumpButton",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 0,
    "bbox_right": 11,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3702231b-f29a-4967-94f6-a35bb2280545",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8970979e-ca6e-4d77-8f3a-438d0fb050fc",
            "compositeImage": {
                "id": "e9d248d7-5866-4a39-a09d-76625ce98015",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3702231b-f29a-4967-94f6-a35bb2280545",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2bf72af6-e13d-4075-9858-c00206e64b41",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3702231b-f29a-4967-94f6-a35bb2280545",
                    "LayerId": "36d13990-f03c-4981-9ca4-482af682a29a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 12,
    "layers": [
        {
            "id": "36d13990-f03c-4981-9ca4-482af682a29a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8970979e-ca6e-4d77-8f3a-438d0fb050fc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 12,
    "xorig": 0,
    "yorig": 0
}