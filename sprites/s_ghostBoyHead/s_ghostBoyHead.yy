{
    "id": "164acc4b-05ca-4800-90b2-52603c349d6d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_ghostBoyHead",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 18,
    "bbox_left": 0,
    "bbox_right": 17,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1f10b178-92a9-499c-bde8-622a2c73956b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "164acc4b-05ca-4800-90b2-52603c349d6d",
            "compositeImage": {
                "id": "27c94a06-265c-4458-b071-fa03223d7447",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f10b178-92a9-499c-bde8-622a2c73956b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "541cc905-dcbb-49bd-9b07-aa822fe16484",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f10b178-92a9-499c-bde8-622a2c73956b",
                    "LayerId": "1d350349-64bc-4d80-944b-7078f18030e7"
                }
            ]
        },
        {
            "id": "f6ecc71f-1080-4319-984c-296ad6d95258",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "164acc4b-05ca-4800-90b2-52603c349d6d",
            "compositeImage": {
                "id": "1dac4b25-944c-4f3f-84d3-a1044d423896",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6ecc71f-1080-4319-984c-296ad6d95258",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f587b3c0-d924-44e7-9273-95bbccf7c366",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6ecc71f-1080-4319-984c-296ad6d95258",
                    "LayerId": "1d350349-64bc-4d80-944b-7078f18030e7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 19,
    "layers": [
        {
            "id": "1d350349-64bc-4d80-944b-7078f18030e7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "164acc4b-05ca-4800-90b2-52603c349d6d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 18,
    "xorig": 10,
    "yorig": 15
}