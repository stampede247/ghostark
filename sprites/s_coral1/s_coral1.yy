{
    "id": "bba4fe40-395e-4e82-8d82-0cc2de406ab3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_coral1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 2,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "eb5fbf35-288d-43e1-862e-7e60ee58d56c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bba4fe40-395e-4e82-8d82-0cc2de406ab3",
            "compositeImage": {
                "id": "8dc34028-0880-4750-9545-53ecd26ccfac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb5fbf35-288d-43e1-862e-7e60ee58d56c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "75b30c81-bce6-40fa-b26f-4e6b8815d962",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb5fbf35-288d-43e1-862e-7e60ee58d56c",
                    "LayerId": "595f42ce-203d-4883-986c-8f55e63b7c25"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 44,
    "layers": [
        {
            "id": "595f42ce-203d-4883-986c-8f55e63b7c25",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bba4fe40-395e-4e82-8d82-0cc2de406ab3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}