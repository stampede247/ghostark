{
    "id": "b2fb38f5-9e32-4d61-99d1-976a07910c69",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_rock2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 2,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "449b75b6-6dd1-4c94-8f4a-3b124034c39b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b2fb38f5-9e32-4d61-99d1-976a07910c69",
            "compositeImage": {
                "id": "7bbc6160-f306-4e34-a1ff-320ea5f9e4f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "449b75b6-6dd1-4c94-8f4a-3b124034c39b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a11afa92-64b7-4600-87cd-d5d96d01f76f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "449b75b6-6dd1-4c94-8f4a-3b124034c39b",
                    "LayerId": "2538b6aa-9ad4-4ba5-b011-473f6ce456f6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "2538b6aa-9ad4-4ba5-b011-473f6ce456f6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b2fb38f5-9e32-4d61-99d1-976a07910c69",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}