{
    "id": "148fd230-fe6d-40f7-a4f3-59ccb5324bfe",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_roomChange",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a0619ac5-64fb-4b92-a5a3-f32cef368f4e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "148fd230-fe6d-40f7-a4f3-59ccb5324bfe",
            "compositeImage": {
                "id": "fefffd58-9f7e-41e5-97c7-5fec3493d66f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0619ac5-64fb-4b92-a5a3-f32cef368f4e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6bb54343-07a5-4530-abf4-7d1cb40a55cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0619ac5-64fb-4b92-a5a3-f32cef368f4e",
                    "LayerId": "ad871da5-7527-4cae-97b4-df6ac6bf4b31"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "ad871da5-7527-4cae-97b4-df6ac6bf4b31",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "148fd230-fe6d-40f7-a4f3-59ccb5324bfe",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}