{
    "id": "ff7b84c0-bdc4-4adc-b3ba-37ff38385ceb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_plank1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 88,
    "bbox_left": 1,
    "bbox_right": 15,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "401329bf-e111-4299-8868-e7cff62945c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ff7b84c0-bdc4-4adc-b3ba-37ff38385ceb",
            "compositeImage": {
                "id": "5b1c021e-c1b6-4852-953e-bc48d2be0adc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "401329bf-e111-4299-8868-e7cff62945c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57b5338c-e5cf-4a3b-b3c6-a10f97ee0cc1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "401329bf-e111-4299-8868-e7cff62945c4",
                    "LayerId": "839c3075-d4d9-476a-ac08-21c84f34b79c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 90,
    "layers": [
        {
            "id": "839c3075-d4d9-476a-ac08-21c84f34b79c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ff7b84c0-bdc4-4adc-b3ba-37ff38385ceb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 17,
    "xorig": 0,
    "yorig": 0
}