{
    "id": "f34f4edb-16a3-4d3f-9713-d9fb29e2a437",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_seaplant2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 77,
    "bbox_left": 3,
    "bbox_right": 96,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "425bd6ae-28de-4b89-999c-9d09ae144cd7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f34f4edb-16a3-4d3f-9713-d9fb29e2a437",
            "compositeImage": {
                "id": "5d7bae9e-7072-4119-85e1-6c55986a5d2b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "425bd6ae-28de-4b89-999c-9d09ae144cd7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2bc5cbcd-721a-4527-82c2-e754be4834a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "425bd6ae-28de-4b89-999c-9d09ae144cd7",
                    "LayerId": "7b36e1a8-26d8-4f0a-a7a7-1a425683a7e8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 80,
    "layers": [
        {
            "id": "7b36e1a8-26d8-4f0a-a7a7-1a425683a7e8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f34f4edb-16a3-4d3f-9713-d9fb29e2a437",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 0,
    "yorig": 0
}