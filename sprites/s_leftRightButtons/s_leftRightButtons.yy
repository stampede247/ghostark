{
    "id": "5615a4e3-6117-4aad-9286-a1443e4f81ef",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_leftRightButtons",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8af38575-a36e-47d5-a771-c83422de1335",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5615a4e3-6117-4aad-9286-a1443e4f81ef",
            "compositeImage": {
                "id": "a303e0cf-5b45-4043-9aa2-db20f5b10b22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8af38575-a36e-47d5-a771-c83422de1335",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cef11189-5376-4d49-89d1-7ff55020c390",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8af38575-a36e-47d5-a771-c83422de1335",
                    "LayerId": "58591fe8-5d24-466f-82b7-3cd534734346"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 12,
    "layers": [
        {
            "id": "58591fe8-5d24-466f-82b7-3cd534734346",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5615a4e3-6117-4aad-9286-a1443e4f81ef",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 0,
    "yorig": 0
}