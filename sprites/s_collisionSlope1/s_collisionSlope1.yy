{
    "id": "2125e333-45f7-4acb-b67d-1d87e9e3f29f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_collisionSlope1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 25,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "070f373b-e209-44c8-94ae-9e2ddef57c79",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2125e333-45f7-4acb-b67d-1d87e9e3f29f",
            "compositeImage": {
                "id": "bdeaa10f-11fb-4fa4-a977-ed2305951924",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "070f373b-e209-44c8-94ae-9e2ddef57c79",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45593342-f98e-404d-abba-75666ccc2f24",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "070f373b-e209-44c8-94ae-9e2ddef57c79",
                    "LayerId": "cf3aeec6-f7c0-461e-acf1-537d903f60e2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 26,
    "layers": [
        {
            "id": "cf3aeec6-f7c0-461e-acf1-537d903f60e2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2125e333-45f7-4acb-b67d-1d87e9e3f29f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 70,
    "yorig": 20
}