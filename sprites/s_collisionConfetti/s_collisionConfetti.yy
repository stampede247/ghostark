{
    "id": "637ddca9-3fb7-4a0b-a6a6-59b4ea28d362",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_collisionConfetti",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bfa404a3-7dc4-4ddc-9434-4da96bb29907",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "637ddca9-3fb7-4a0b-a6a6-59b4ea28d362",
            "compositeImage": {
                "id": "1d2446ca-3702-4097-a134-cd7bca5f7edd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bfa404a3-7dc4-4ddc-9434-4da96bb29907",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "921940c2-7657-4644-83d0-ff73771bec32",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bfa404a3-7dc4-4ddc-9434-4da96bb29907",
                    "LayerId": "e479b5a4-8334-4c62-bba8-bf0c028ef91a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "e479b5a4-8334-4c62-bba8-bf0c028ef91a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "637ddca9-3fb7-4a0b-a6a6-59b4ea28d362",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 11,
    "yorig": 5
}