{
    "id": "26ae49fc-f119-4a87-bc84-2bd649700c79",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_boat3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 33,
    "bbox_left": 4,
    "bbox_right": 31,
    "bbox_top": 2,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "90476cab-e57d-441d-952d-eea04b19021e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "26ae49fc-f119-4a87-bc84-2bd649700c79",
            "compositeImage": {
                "id": "758607fe-152b-446c-8ad4-7dd0dd4d5393",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90476cab-e57d-441d-952d-eea04b19021e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ce26adb-891a-42a0-8961-cfd1b651804a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90476cab-e57d-441d-952d-eea04b19021e",
                    "LayerId": "ec792c00-ab52-463b-9cba-1637750f7a2c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 69,
    "layers": [
        {
            "id": "ec792c00-ab52-463b-9cba-1637750f7a2c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "26ae49fc-f119-4a87-bc84-2bd649700c79",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 70,
    "xorig": 9,
    "yorig": 2
}