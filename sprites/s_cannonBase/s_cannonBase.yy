{
    "id": "a96b95d3-90ea-4ac3-8dce-d379eaa9bbeb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_cannonBase",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 0,
    "bbox_right": 30,
    "bbox_top": 17,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "990ff4a2-a374-49b1-b230-0baf5b47a2bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a96b95d3-90ea-4ac3-8dce-d379eaa9bbeb",
            "compositeImage": {
                "id": "1923d79b-13bf-451f-877d-2f51fea13f49",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "990ff4a2-a374-49b1-b230-0baf5b47a2bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a4b1752-42a0-4038-ac79-d8343c1c76bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "990ff4a2-a374-49b1-b230-0baf5b47a2bd",
                    "LayerId": "362c4382-7a8b-43c6-ac09-eae7cdf46b98"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 25,
    "layers": [
        {
            "id": "362c4382-7a8b-43c6-ac09-eae7cdf46b98",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a96b95d3-90ea-4ac3-8dce-d379eaa9bbeb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}