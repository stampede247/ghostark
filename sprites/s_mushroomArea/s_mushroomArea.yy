{
    "id": "8ab56208-ed10-4ea8-80f4-d9fda0f083de",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_mushroomArea",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "68a7c228-39ff-4978-8dcf-1d5c8737128e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8ab56208-ed10-4ea8-80f4-d9fda0f083de",
            "compositeImage": {
                "id": "8fb263cb-b3ff-4973-a703-2a380e1f7707",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68a7c228-39ff-4978-8dcf-1d5c8737128e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85fb7597-bb06-4665-8094-a792daeb1b9e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68a7c228-39ff-4978-8dcf-1d5c8737128e",
                    "LayerId": "17210c9e-be00-4cd9-a977-c45974cf47d1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "17210c9e-be00-4cd9-a977-c45974cf47d1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8ab56208-ed10-4ea8-80f4-d9fda0f083de",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}