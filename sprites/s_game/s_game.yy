{
    "id": "426d64bf-c92a-4950-9bd8-f93e5d128566",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_game",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 61,
    "bbox_left": 2,
    "bbox_right": 62,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "05952012-bafa-44d9-a90b-975985f9f710",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "426d64bf-c92a-4950-9bd8-f93e5d128566",
            "compositeImage": {
                "id": "b344f26d-8d95-45eb-adb8-51331ca3351c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05952012-bafa-44d9-a90b-975985f9f710",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4bbbcb88-e03e-4f87-91a0-e9423f1c3af7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05952012-bafa-44d9-a90b-975985f9f710",
                    "LayerId": "d5370328-e998-43dd-9f36-79cfc64a6be4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "d5370328-e998-43dd-9f36-79cfc64a6be4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "426d64bf-c92a-4950-9bd8-f93e5d128566",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}