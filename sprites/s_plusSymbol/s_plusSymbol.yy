{
    "id": "b77f66e5-1a37-453c-82eb-4566350b9e26",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_plusSymbol",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 0,
    "bbox_right": 12,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bc0b3950-3af0-49c2-a591-06fcbcdd530c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b77f66e5-1a37-453c-82eb-4566350b9e26",
            "compositeImage": {
                "id": "c1173aea-ed3d-4975-b30c-581c2f9c9225",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc0b3950-3af0-49c2-a591-06fcbcdd530c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d368d82-a4a9-4f24-88af-47e1050708bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc0b3950-3af0-49c2-a591-06fcbcdd530c",
                    "LayerId": "bccdb5ef-d556-4e58-bf74-cebfa6f5b982"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 13,
    "layers": [
        {
            "id": "bccdb5ef-d556-4e58-bf74-cebfa6f5b982",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b77f66e5-1a37-453c-82eb-4566350b9e26",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 13,
    "xorig": 6,
    "yorig": 6
}