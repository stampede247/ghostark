{
    "id": "311304fd-a6ae-4641-ac61-9647211e0dc5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_collisionPlatform",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "116257d1-6c4e-474a-96c8-b76b59654000",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "311304fd-a6ae-4641-ac61-9647211e0dc5",
            "compositeImage": {
                "id": "4f46404d-59bd-492f-8982-5b4c816eac34",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "116257d1-6c4e-474a-96c8-b76b59654000",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c8f7c42-4712-4da9-8786-802aec164dca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "116257d1-6c4e-474a-96c8-b76b59654000",
                    "LayerId": "97cb5c0f-c902-49ca-910b-3b0d2a8cb1a1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 12,
    "layers": [
        {
            "id": "97cb5c0f-c902-49ca-910b-3b0d2a8cb1a1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "311304fd-a6ae-4641-ac61-9647211e0dc5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}