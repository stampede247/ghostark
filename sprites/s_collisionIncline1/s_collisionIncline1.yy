{
    "id": "5231fad4-c049-4a31-850b-84e425dc9ef6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_collisionIncline1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a2f5da17-b418-455c-8c34-1b609b16ee54",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5231fad4-c049-4a31-850b-84e425dc9ef6",
            "compositeImage": {
                "id": "c5ff0f40-3aae-4e8c-ba9c-9f550b9d33dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2f5da17-b418-455c-8c34-1b609b16ee54",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e97d5ec1-0a4e-4df5-a75d-af2b958d0723",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2f5da17-b418-455c-8c34-1b609b16ee54",
                    "LayerId": "1a7b98f7-c079-4cab-ade8-340c4d34c531"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "1a7b98f7-c079-4cab-ade8-340c4d34c531",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5231fad4-c049-4a31-850b-84e425dc9ef6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}