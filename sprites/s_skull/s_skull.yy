{
    "id": "e45e195b-e72c-44ae-abc5-f23257f9465f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_skull",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 1,
    "bbox_right": 22,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8f917247-6422-4332-b1da-7548b3d422db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e45e195b-e72c-44ae-abc5-f23257f9465f",
            "compositeImage": {
                "id": "c7e43431-08c6-4193-a0d0-2e5322798b5b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f917247-6422-4332-b1da-7548b3d422db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "922578b1-9b19-444b-adf0-df12b7f299b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f917247-6422-4332-b1da-7548b3d422db",
                    "LayerId": "838b7a76-64cf-49b2-b710-efe228504ddd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "838b7a76-64cf-49b2-b710-efe228504ddd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e45e195b-e72c-44ae-abc5-f23257f9465f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 12
}