{
    "id": "d5130354-81a9-4921-ac91-ee5efde5838a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_ghostBoyBodyCrouch",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 1,
    "bbox_right": 16,
    "bbox_top": 6,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c41f90dc-fdbf-434b-b325-a5855c7d5a92",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5130354-81a9-4921-ac91-ee5efde5838a",
            "compositeImage": {
                "id": "17945fe6-d277-470f-97f3-abd2f47ac389",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c41f90dc-fdbf-434b-b325-a5855c7d5a92",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b1a70ca-30c9-4de3-bfcf-be9220be3a6a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c41f90dc-fdbf-434b-b325-a5855c7d5a92",
                    "LayerId": "d6913d16-dcf7-4c1d-9dac-b9184e27826f"
                }
            ]
        },
        {
            "id": "85141ebd-1673-4567-87cf-aa6f06de28df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5130354-81a9-4921-ac91-ee5efde5838a",
            "compositeImage": {
                "id": "083cb322-c8d0-403d-9f24-55aedb46ce06",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85141ebd-1673-4567-87cf-aa6f06de28df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed944e23-5425-4f57-b62c-d46be06a975b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85141ebd-1673-4567-87cf-aa6f06de28df",
                    "LayerId": "d6913d16-dcf7-4c1d-9dac-b9184e27826f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 22,
    "layers": [
        {
            "id": "d6913d16-dcf7-4c1d-9dac-b9184e27826f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d5130354-81a9-4921-ac91-ee5efde5838a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 18,
    "xorig": 9,
    "yorig": 11
}