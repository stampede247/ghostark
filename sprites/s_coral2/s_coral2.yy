{
    "id": "32c08c85-82b5-4804-8d12-de70e7fd51d5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_coral2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 2,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e2f40505-5fd1-4210-b1fc-34a8817c9640",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "32c08c85-82b5-4804-8d12-de70e7fd51d5",
            "compositeImage": {
                "id": "96e5203f-978d-44de-906f-69b72bfcdfe9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2f40505-5fd1-4210-b1fc-34a8817c9640",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "876b3847-6cd9-4e41-8f3b-13dff8e394bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2f40505-5fd1-4210-b1fc-34a8817c9640",
                    "LayerId": "3fca4419-331e-4891-bf06-50c5ba796805"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "3fca4419-331e-4891-bf06-50c5ba796805",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "32c08c85-82b5-4804-8d12-de70e7fd51d5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 0,
    "yorig": 0
}