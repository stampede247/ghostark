{
    "id": "6e901d12-67b1-40ce-a592-948cc5947b91",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_coral3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 2,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a84e97ef-9f5d-47ee-a973-f4cf4d5a4e32",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6e901d12-67b1-40ce-a592-948cc5947b91",
            "compositeImage": {
                "id": "56110dc5-8d34-498d-ac13-7f642b805bc4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a84e97ef-9f5d-47ee-a973-f4cf4d5a4e32",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97adff9c-ece6-4c7d-9e94-c573ec76075b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a84e97ef-9f5d-47ee-a973-f4cf4d5a4e32",
                    "LayerId": "0c8e4102-ef14-4acf-8fe2-8b3eea5e25db"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 59,
    "layers": [
        {
            "id": "0c8e4102-ef14-4acf-8fe2-8b3eea5e25db",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6e901d12-67b1-40ce-a592-948cc5947b91",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 19,
    "xorig": 0,
    "yorig": 0
}