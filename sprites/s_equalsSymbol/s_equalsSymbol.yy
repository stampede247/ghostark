{
    "id": "6109dcd0-f5a2-4fd5-9f44-fa147ab0d173",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_equalsSymbol",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 8,
    "bbox_left": 0,
    "bbox_right": 11,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3dc28341-c1ab-4b12-b546-d4acb9f9cd6f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6109dcd0-f5a2-4fd5-9f44-fa147ab0d173",
            "compositeImage": {
                "id": "e7597e60-9966-4321-bce5-acc20d6e20a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3dc28341-c1ab-4b12-b546-d4acb9f9cd6f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "977f0091-b787-4cbd-98e6-e4f6102329e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3dc28341-c1ab-4b12-b546-d4acb9f9cd6f",
                    "LayerId": "12806d9c-fab1-47a4-9943-2af5cfcc4da7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 9,
    "layers": [
        {
            "id": "12806d9c-fab1-47a4-9943-2af5cfcc4da7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6109dcd0-f5a2-4fd5-9f44-fa147ab0d173",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 12,
    "xorig": 5,
    "yorig": 4
}