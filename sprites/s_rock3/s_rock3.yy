{
    "id": "c14313a5-268e-4695-af6f-aa7e2962bd8d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_rock3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 2,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "02ee50dc-53ac-4f64-a22c-0dec8e451df7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c14313a5-268e-4695-af6f-aa7e2962bd8d",
            "compositeImage": {
                "id": "b4244ea3-20b0-4f7e-80b3-70ff19449a78",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02ee50dc-53ac-4f64-a22c-0dec8e451df7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4c8e157-d97f-4ec1-a818-341e6b047583",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02ee50dc-53ac-4f64-a22c-0dec8e451df7",
                    "LayerId": "0ca5b937-02b2-4373-be3d-26a2e5f22f2f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "0ca5b937-02b2-4373-be3d-26a2e5f22f2f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c14313a5-268e-4695-af6f-aa7e2962bd8d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 0,
    "yorig": 0
}