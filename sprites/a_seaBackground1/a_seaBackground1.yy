{
    "id": "eedb6feb-3644-4eaa-977c-ed5be3a987dc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "a_seaBackground1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 511,
    "bbox_left": 0,
    "bbox_right": 511,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2910faa2-60f9-46b3-b7b2-dec6a9fe2a89",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eedb6feb-3644-4eaa-977c-ed5be3a987dc",
            "compositeImage": {
                "id": "f03ee655-c3f5-455e-bc9a-b0c209cce354",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2910faa2-60f9-46b3-b7b2-dec6a9fe2a89",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dea1b98b-9a2d-4909-8126-f5bbf545da88",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2910faa2-60f9-46b3-b7b2-dec6a9fe2a89",
                    "LayerId": "90ea5d48-e8db-4199-9fad-f744ab5b0539"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "90ea5d48-e8db-4199-9fad-f744ab5b0539",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "eedb6feb-3644-4eaa-977c-ed5be3a987dc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": 0,
    "yorig": 0
}