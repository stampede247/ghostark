{
    "id": "2cd01f04-774d-4002-9e03-ef7e860a24a6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_seaplant3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 96,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3f6b2032-bc11-4614-806f-9e3360780990",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2cd01f04-774d-4002-9e03-ef7e860a24a6",
            "compositeImage": {
                "id": "dcd10582-14fd-443e-b319-2a35b88e45ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f6b2032-bc11-4614-806f-9e3360780990",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15b9af94-45dc-4630-b661-6816861064de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f6b2032-bc11-4614-806f-9e3360780990",
                    "LayerId": "e9d9ba63-0f71-443e-ab74-ea499673afcb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 97,
    "layers": [
        {
            "id": "e9d9ba63-0f71-443e-ab74-ea499673afcb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2cd01f04-774d-4002-9e03-ef7e860a24a6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 0,
    "yorig": 0
}