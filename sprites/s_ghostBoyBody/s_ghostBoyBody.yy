{
    "id": "dcad2fc4-5c24-44da-973b-8aa05a263b00",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_ghostBoyBody",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 1,
    "bbox_right": 16,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "26b2887d-c036-411e-98f6-0fd050d542bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dcad2fc4-5c24-44da-973b-8aa05a263b00",
            "compositeImage": {
                "id": "5b28571e-ff4e-4c76-a60b-90e7fa2ecbe7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26b2887d-c036-411e-98f6-0fd050d542bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2a50fef-8226-47d3-93d9-be18490288c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26b2887d-c036-411e-98f6-0fd050d542bf",
                    "LayerId": "6bffdd7c-2aeb-43dc-a32e-d2ed9b3ef451"
                }
            ]
        },
        {
            "id": "feef441e-a55a-4e58-8ae8-9245c675d798",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dcad2fc4-5c24-44da-973b-8aa05a263b00",
            "compositeImage": {
                "id": "8c75229b-3d91-4f80-9cea-9219be7af471",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "feef441e-a55a-4e58-8ae8-9245c675d798",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "217b46c2-819c-4f93-8ee6-a9ce5c3af0d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "feef441e-a55a-4e58-8ae8-9245c675d798",
                    "LayerId": "6bffdd7c-2aeb-43dc-a32e-d2ed9b3ef451"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 22,
    "layers": [
        {
            "id": "6bffdd7c-2aeb-43dc-a32e-d2ed9b3ef451",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dcad2fc4-5c24-44da-973b-8aa05a263b00",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 18,
    "xorig": 9,
    "yorig": 11
}