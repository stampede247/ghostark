{
    "id": "e5b6b295-d179-44f0-aeda-434e953321b1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_seaMushroom1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cee5bad9-844e-4467-92ac-160bb7515fd3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5b6b295-d179-44f0-aeda-434e953321b1",
            "compositeImage": {
                "id": "2010f3ab-1c42-49c0-884c-09717197ad9e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cee5bad9-844e-4467-92ac-160bb7515fd3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a933ddf9-42c2-442d-87ca-9d6c6488ed07",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cee5bad9-844e-4467-92ac-160bb7515fd3",
                    "LayerId": "35bc4fbb-ac04-4508-96a4-ad6b1b6e5553"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "35bc4fbb-ac04-4508-96a4-ad6b1b6e5553",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e5b6b295-d179-44f0-aeda-434e953321b1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 15,
    "xorig": 0,
    "yorig": 0
}