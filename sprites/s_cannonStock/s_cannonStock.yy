{
    "id": "689c2d03-eecc-4a3f-840d-8eaeeaac1ae1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_cannonStock",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 24,
    "bboxmode": 2,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f7620979-783a-4aef-b60e-87cd2c1e2b1a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "689c2d03-eecc-4a3f-840d-8eaeeaac1ae1",
            "compositeImage": {
                "id": "4f65713c-7a85-4bfa-8327-95a70478537a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7620979-783a-4aef-b60e-87cd2c1e2b1a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46cc9e64-8796-46fb-9dde-3500c8855f44",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7620979-783a-4aef-b60e-87cd2c1e2b1a",
                    "LayerId": "83126996-7334-43d0-9b59-452a4526ae72"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "83126996-7334-43d0-9b59-452a4526ae72",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "689c2d03-eecc-4a3f-840d-8eaeeaac1ae1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 82,
    "xorig": 0,
    "yorig": 0
}