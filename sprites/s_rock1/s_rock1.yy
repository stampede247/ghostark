{
    "id": "d5629505-8650-4e45-a4c2-366d35165ccb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_rock1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 2,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "65346f17-86dc-42f3-85ff-ba8244936eec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5629505-8650-4e45-a4c2-366d35165ccb",
            "compositeImage": {
                "id": "0e6bdb1b-4549-4659-bb01-b7d5ef9573e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65346f17-86dc-42f3-85ff-ba8244936eec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3583b324-8725-4654-a652-677bedabff37",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65346f17-86dc-42f3-85ff-ba8244936eec",
                    "LayerId": "43e39eea-df37-4edd-bc79-5f45b88a3ffa"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "43e39eea-df37-4edd-bc79-5f45b88a3ffa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d5629505-8650-4e45-a4c2-366d35165ccb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}