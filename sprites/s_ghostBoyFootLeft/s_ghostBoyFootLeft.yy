{
    "id": "1667826d-ae77-4a17-becc-effc6a400357",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_ghostBoyFootLeft",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 6,
    "bbox_left": 1,
    "bbox_right": 10,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e6de8838-0222-470b-b911-183ca116c3cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1667826d-ae77-4a17-becc-effc6a400357",
            "compositeImage": {
                "id": "6242c1e9-c80c-478c-be86-b7a831471e15",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6de8838-0222-470b-b911-183ca116c3cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea9b6b6e-fce1-46ae-8a3d-977a3e86b492",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6de8838-0222-470b-b911-183ca116c3cb",
                    "LayerId": "fdedd1aa-b5b7-4bb2-bb12-8a7706f6d52d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "fdedd1aa-b5b7-4bb2-bb12-8a7706f6d52d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1667826d-ae77-4a17-becc-effc6a400357",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 12,
    "xorig": 6,
    "yorig": 5
}