{
    "id": "23312597-58ca-49b9-b871-7af87d384cdc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_seaplant1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 85,
    "bbox_left": 2,
    "bbox_right": 47,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "569f35ad-61f8-41c1-8bd7-cf662df89b32",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "23312597-58ca-49b9-b871-7af87d384cdc",
            "compositeImage": {
                "id": "3369181e-a739-4ec6-a6e7-22f6ccae9190",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "569f35ad-61f8-41c1-8bd7-cf662df89b32",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3060d25-d801-4f97-88f6-757cbde8105f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "569f35ad-61f8-41c1-8bd7-cf662df89b32",
                    "LayerId": "95ec9ad4-9bad-430e-914b-f5aa0fbc33bf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 86,
    "layers": [
        {
            "id": "95ec9ad4-9bad-430e-914b-f5aa0fbc33bf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "23312597-58ca-49b9-b871-7af87d384cdc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 0,
    "yorig": 0
}