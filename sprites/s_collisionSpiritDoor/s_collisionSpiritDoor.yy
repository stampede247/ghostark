{
    "id": "28abb8a1-4c78-4524-96c1-874d7bdbe69f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_collisionSpiritDoor",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e1f20cf1-b8e1-4eac-ba11-7cc45ab040cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28abb8a1-4c78-4524-96c1-874d7bdbe69f",
            "compositeImage": {
                "id": "bf901a5f-19e5-4900-8f91-f00b04fbecd5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1f20cf1-b8e1-4eac-ba11-7cc45ab040cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2fd505ff-4e27-4ec5-9806-9034d36994cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1f20cf1-b8e1-4eac-ba11-7cc45ab040cc",
                    "LayerId": "017f8f43-a39b-4ba7-b6e1-9e76ed2ced8f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "017f8f43-a39b-4ba7-b6e1-9e76ed2ced8f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "28abb8a1-4c78-4524-96c1-874d7bdbe69f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 0,
    "yorig": 0
}