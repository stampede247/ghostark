{
    "id": "b808adaa-f3fa-49e3-a9c6-20f2289cf0a2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_skullActive",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 1,
    "bbox_right": 22,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6c2cd8fa-8c3d-433d-ace7-8bcb940ce925",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b808adaa-f3fa-49e3-a9c6-20f2289cf0a2",
            "compositeImage": {
                "id": "173199f0-6d63-4293-8602-5a9375c2455a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c2cd8fa-8c3d-433d-ace7-8bcb940ce925",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dad1b667-a856-4131-b65f-acd09351734f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c2cd8fa-8c3d-433d-ace7-8bcb940ce925",
                    "LayerId": "600e0502-a211-48ac-b2a7-a928a93b2862"
                }
            ]
        },
        {
            "id": "7d17715f-9c9f-47ca-8790-e70988505e45",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b808adaa-f3fa-49e3-a9c6-20f2289cf0a2",
            "compositeImage": {
                "id": "a96cb13d-443f-4d48-8337-f15722646302",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d17715f-9c9f-47ca-8790-e70988505e45",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ffa28f4-3918-4256-9656-92142525e6b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d17715f-9c9f-47ca-8790-e70988505e45",
                    "LayerId": "600e0502-a211-48ac-b2a7-a928a93b2862"
                }
            ]
        },
        {
            "id": "a5b5dec5-1f9a-48d9-a3eb-e56bf7b5d681",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b808adaa-f3fa-49e3-a9c6-20f2289cf0a2",
            "compositeImage": {
                "id": "5ef18bda-5e82-4faf-b8a5-84bae7bd647d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5b5dec5-1f9a-48d9-a3eb-e56bf7b5d681",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48d2860b-54c1-4cfb-ad5a-0bea1f993565",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5b5dec5-1f9a-48d9-a3eb-e56bf7b5d681",
                    "LayerId": "600e0502-a211-48ac-b2a7-a928a93b2862"
                }
            ]
        },
        {
            "id": "e01a15ae-838c-4e02-899e-f0031ddebef8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b808adaa-f3fa-49e3-a9c6-20f2289cf0a2",
            "compositeImage": {
                "id": "b9becda8-7e7b-4e5f-b96e-742415170218",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e01a15ae-838c-4e02-899e-f0031ddebef8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c17dcbaf-b7ee-4249-911b-923ee2029f66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e01a15ae-838c-4e02-899e-f0031ddebef8",
                    "LayerId": "600e0502-a211-48ac-b2a7-a928a93b2862"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "600e0502-a211-48ac-b2a7-a928a93b2862",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b808adaa-f3fa-49e3-a9c6-20f2289cf0a2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 12
}