{
    "id": "73a997bd-51e8-4a1c-949a-87b90833f5d7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_rock4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 80,
    "bbox_left": 0,
    "bbox_right": 48,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "82ccc053-8f85-4df4-ae66-e06df01ca219",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "73a997bd-51e8-4a1c-949a-87b90833f5d7",
            "compositeImage": {
                "id": "d656b71a-bac8-4c34-ac92-97fd24840244",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82ccc053-8f85-4df4-ae66-e06df01ca219",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b360b6ee-8c7a-449c-96a0-a37917878fe1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82ccc053-8f85-4df4-ae66-e06df01ca219",
                    "LayerId": "3c8d8961-b997-477b-89c8-c81582b281e4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 81,
    "layers": [
        {
            "id": "3c8d8961-b997-477b-89c8-c81582b281e4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "73a997bd-51e8-4a1c-949a-87b90833f5d7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 49,
    "xorig": 0,
    "yorig": 0
}