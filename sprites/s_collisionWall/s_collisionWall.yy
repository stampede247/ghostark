{
    "id": "273286fd-9d5f-428c-93ec-a9b6429afc4b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_collisionWall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 5,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3e2f05e9-cdb1-4cbd-91be-015bd509429a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "273286fd-9d5f-428c-93ec-a9b6429afc4b",
            "compositeImage": {
                "id": "25129ef0-9619-4cb7-bf50-fd9dbfb54ed8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e2f05e9-cdb1-4cbd-91be-015bd509429a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ecfe40fa-94d5-4bce-81ad-aef509bf3247",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e2f05e9-cdb1-4cbd-91be-015bd509429a",
                    "LayerId": "adbbc43e-fcb6-4f4e-9536-0f09db254319"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "adbbc43e-fcb6-4f4e-9536-0f09db254319",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "273286fd-9d5f-428c-93ec-a9b6429afc4b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}