{
    "id": "c19d0e19-c702-4fff-8905-6d1d3a2705a2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_boat2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 33,
    "bbox_left": 4,
    "bbox_right": 31,
    "bbox_top": 2,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9d6ba16b-6d82-47d1-92c5-40cde2a2f4b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c19d0e19-c702-4fff-8905-6d1d3a2705a2",
            "compositeImage": {
                "id": "5fa3cf6d-e2e2-40c6-be1d-0d94da564dfc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d6ba16b-6d82-47d1-92c5-40cde2a2f4b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e391599-bdba-4bb0-b25b-fe8d6366ca6f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d6ba16b-6d82-47d1-92c5-40cde2a2f4b0",
                    "LayerId": "0f64e0d3-ae15-421b-bd38-4fa16f7a5f42"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 54,
    "layers": [
        {
            "id": "0f64e0d3-ae15-421b-bd38-4fa16f7a5f42",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c19d0e19-c702-4fff-8905-6d1d3a2705a2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 112,
    "xorig": 11,
    "yorig": 1
}