{
    "id": "4bc1c60a-a64e-4572-8414-da65ca509236",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_partDot",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 4,
    "bbox_left": 3,
    "bbox_right": 4,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "53e6b85a-1247-4afe-9da4-98009d431abf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4bc1c60a-a64e-4572-8414-da65ca509236",
            "compositeImage": {
                "id": "ad431012-c707-4ac2-abd3-1e99a7f24cf9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "53e6b85a-1247-4afe-9da4-98009d431abf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ba34a07-b12a-4e7d-a6c3-144f6e71a66d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "53e6b85a-1247-4afe-9da4-98009d431abf",
                    "LayerId": "9acee528-e963-460c-a843-a655fcc66c0b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "9acee528-e963-460c-a843-a655fcc66c0b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4bc1c60a-a64e-4572-8414-da65ca509236",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 4
}