{
    "id": "599b26a8-0998-4fb4-ba98-7733ea47b62b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_shader1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bc1c0fb5-fab0-4043-838b-54abd1801922",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "599b26a8-0998-4fb4-ba98-7733ea47b62b",
            "compositeImage": {
                "id": "5a6da7d4-4bd8-4a19-a904-65a088aac81f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc1c0fb5-fab0-4043-838b-54abd1801922",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ecdb73af-62fe-4825-8a3c-287a0823766d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc1c0fb5-fab0-4043-838b-54abd1801922",
                    "LayerId": "d1f40c7f-ef2f-4e8c-a63a-0d580db3697f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "d1f40c7f-ef2f-4e8c-a63a-0d580db3697f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "599b26a8-0998-4fb4-ba98-7733ea47b62b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}