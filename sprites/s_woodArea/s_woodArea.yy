{
    "id": "2e52e7c3-18a4-443f-b0a8-bcf42cc874c1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_woodArea",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e55f0326-bcf6-41be-a71b-4cf9f8e444cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e52e7c3-18a4-443f-b0a8-bcf42cc874c1",
            "compositeImage": {
                "id": "c952ae16-d988-46bf-bf73-d829cee0da1f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e55f0326-bcf6-41be-a71b-4cf9f8e444cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d348919-2c45-4bf5-8d30-599567e79b40",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e55f0326-bcf6-41be-a71b-4cf9f8e444cc",
                    "LayerId": "0dd9c3c1-a5bf-4f46-b9b3-463be65f92fd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "0dd9c3c1-a5bf-4f46-b9b3-463be65f92fd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2e52e7c3-18a4-443f-b0a8-bcf42cc874c1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}