{
    "id": "6a669084-0966-4b53-add2-c92f316181e1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_confetti",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 1,
    "bbox_right": 7,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fd7a2882-15e2-477a-8177-7796cf272449",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a669084-0966-4b53-add2-c92f316181e1",
            "compositeImage": {
                "id": "c18293bf-b16e-485b-a579-63be82a34596",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd7a2882-15e2-477a-8177-7796cf272449",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46b20dee-1ff6-433e-a802-bfdfa0c7fbdd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd7a2882-15e2-477a-8177-7796cf272449",
                    "LayerId": "3689bf63-7f8b-4853-95ae-4447072b7b3d"
                }
            ]
        },
        {
            "id": "2d13c1a4-7ff8-4eac-b17d-a88fff1ad0b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a669084-0966-4b53-add2-c92f316181e1",
            "compositeImage": {
                "id": "e0c6110f-83ff-44c3-9ad7-daf84ea40afe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d13c1a4-7ff8-4eac-b17d-a88fff1ad0b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94c5218c-701b-4db2-b413-317c18fa50aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d13c1a4-7ff8-4eac-b17d-a88fff1ad0b0",
                    "LayerId": "3689bf63-7f8b-4853-95ae-4447072b7b3d"
                }
            ]
        },
        {
            "id": "33e5a1b1-9915-44ec-9f2a-45633ff544b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a669084-0966-4b53-add2-c92f316181e1",
            "compositeImage": {
                "id": "f85bf4ef-1830-4e50-8088-3300a391bb30",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33e5a1b1-9915-44ec-9f2a-45633ff544b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "398c29fe-7873-4c58-83bb-f2cf4d231652",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33e5a1b1-9915-44ec-9f2a-45633ff544b8",
                    "LayerId": "3689bf63-7f8b-4853-95ae-4447072b7b3d"
                }
            ]
        },
        {
            "id": "57923a45-d1bb-4e20-a65b-633f32163c33",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a669084-0966-4b53-add2-c92f316181e1",
            "compositeImage": {
                "id": "209324c6-c80e-4948-948d-c14e314149f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57923a45-d1bb-4e20-a65b-633f32163c33",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9925be00-4753-4b74-9e1a-069b70b9fc9e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57923a45-d1bb-4e20-a65b-633f32163c33",
                    "LayerId": "3689bf63-7f8b-4853-95ae-4447072b7b3d"
                }
            ]
        },
        {
            "id": "59b09f48-0832-451e-8111-a78f92a47a86",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a669084-0966-4b53-add2-c92f316181e1",
            "compositeImage": {
                "id": "59677c7f-89ae-4fe8-90f3-c315f84a076f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59b09f48-0832-451e-8111-a78f92a47a86",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a29d82cf-ccb4-4651-be5b-af0fb4948c39",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59b09f48-0832-451e-8111-a78f92a47a86",
                    "LayerId": "3689bf63-7f8b-4853-95ae-4447072b7b3d"
                }
            ]
        },
        {
            "id": "473353b9-2424-485c-baf2-2090c4e1c763",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a669084-0966-4b53-add2-c92f316181e1",
            "compositeImage": {
                "id": "70c54580-d7ca-4be7-9ec5-f3a500a14e2a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "473353b9-2424-485c-baf2-2090c4e1c763",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4c8d9b2-032f-4681-b944-a35cfd71f75d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "473353b9-2424-485c-baf2-2090c4e1c763",
                    "LayerId": "3689bf63-7f8b-4853-95ae-4447072b7b3d"
                }
            ]
        },
        {
            "id": "f1079ac7-5f32-4770-b792-52491f7fffef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a669084-0966-4b53-add2-c92f316181e1",
            "compositeImage": {
                "id": "223d97cb-e2e4-4726-a5d0-2f480be6f557",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1079ac7-5f32-4770-b792-52491f7fffef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b7aa628-8a3e-4187-b1f4-2e9380f8b343",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1079ac7-5f32-4770-b792-52491f7fffef",
                    "LayerId": "3689bf63-7f8b-4853-95ae-4447072b7b3d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "3689bf63-7f8b-4853-95ae-4447072b7b3d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6a669084-0966-4b53-add2-c92f316181e1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}