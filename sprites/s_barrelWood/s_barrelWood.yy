{
    "id": "26bffce1-e2ff-4b21-8009-e5033e65aaaa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_barrelWood",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 33,
    "bbox_left": 4,
    "bbox_right": 31,
    "bbox_top": 2,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "66c5fded-c014-4a48-a1f9-10f0c73d51c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "26bffce1-e2ff-4b21-8009-e5033e65aaaa",
            "compositeImage": {
                "id": "81a478ed-6e0b-44b2-b689-bfc8466ddf41",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66c5fded-c014-4a48-a1f9-10f0c73d51c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3347cb7-7c95-4847-8797-a145ba18a83d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66c5fded-c014-4a48-a1f9-10f0c73d51c5",
                    "LayerId": "50ca4c47-0602-4a8e-b3f8-630add98d4b6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 36,
    "layers": [
        {
            "id": "50ca4c47-0602-4a8e-b3f8-630add98d4b6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "26bffce1-e2ff-4b21-8009-e5033e65aaaa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 36,
    "xorig": 4,
    "yorig": 2
}