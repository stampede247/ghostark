{
    "id": "505be4a7-2586-4f3c-aeb6-37e537fbc981",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_shader2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3e4b136f-87bb-4a95-a034-0a95ed5ca9b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "505be4a7-2586-4f3c-aeb6-37e537fbc981",
            "compositeImage": {
                "id": "c5a822dc-59e9-44ca-a9cd-da59f60c7772",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e4b136f-87bb-4a95-a034-0a95ed5ca9b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40a52278-9679-4732-8599-041455e172c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e4b136f-87bb-4a95-a034-0a95ed5ca9b0",
                    "LayerId": "42acaa1f-7d8f-456d-82d2-1b6f1270513e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "42acaa1f-7d8f-456d-82d2-1b6f1270513e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "505be4a7-2586-4f3c-aeb6-37e537fbc981",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}