{
    "id": "489579b7-f55d-4796-99ed-09054eb9f2a3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_barrelMetal",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 33,
    "bbox_left": 4,
    "bbox_right": 31,
    "bbox_top": 2,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bd038cea-fdc3-414f-9667-7ba1942af6e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "489579b7-f55d-4796-99ed-09054eb9f2a3",
            "compositeImage": {
                "id": "9202a9f9-f510-4c77-a837-31f6b43c7ce5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd038cea-fdc3-414f-9667-7ba1942af6e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1239027-d4a6-45de-9b95-6b5c2020695c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd038cea-fdc3-414f-9667-7ba1942af6e9",
                    "LayerId": "bd24540a-e035-405c-b77f-51476a611d50"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 36,
    "layers": [
        {
            "id": "bd24540a-e035-405c-b77f-51476a611d50",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "489579b7-f55d-4796-99ed-09054eb9f2a3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 36,
    "xorig": 4,
    "yorig": 2
}