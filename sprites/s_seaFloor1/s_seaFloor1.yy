{
    "id": "0ea86390-1220-4043-8b06-0281e93e2c55",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_seaFloor1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 75,
    "bbox_left": 0,
    "bbox_right": 95,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8b917172-bdeb-4757-8a32-3954ffe090c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0ea86390-1220-4043-8b06-0281e93e2c55",
            "compositeImage": {
                "id": "6ed3255c-63f1-4626-8cbb-6c3d88f5e8f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b917172-bdeb-4757-8a32-3954ffe090c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bcd7b8cc-1735-4cd6-8217-9edbaa87bf6f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b917172-bdeb-4757-8a32-3954ffe090c6",
                    "LayerId": "8003d0bf-c4d8-4b28-b28d-1ba433f175d3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 76,
    "layers": [
        {
            "id": "8003d0bf-c4d8-4b28-b28d-1ba433f175d3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0ea86390-1220-4043-8b06-0281e93e2c55",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 0,
    "yorig": 0
}