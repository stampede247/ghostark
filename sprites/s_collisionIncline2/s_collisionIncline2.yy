{
    "id": "2c97acff-7c2f-49ab-8d30-f0192d94e720",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_collisionIncline2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2ca74c49-e040-47ff-83f2-3bbfe53f33c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c97acff-7c2f-49ab-8d30-f0192d94e720",
            "compositeImage": {
                "id": "25789c5c-4cdf-4ac9-b031-295779d52e80",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ca74c49-e040-47ff-83f2-3bbfe53f33c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "029c8d49-bb5f-4ec2-86af-0c25adf3e4aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ca74c49-e040-47ff-83f2-3bbfe53f33c3",
                    "LayerId": "44f9d543-ac84-4de4-b05b-643517357ef4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "44f9d543-ac84-4de4-b05b-643517357ef4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2c97acff-7c2f-49ab-8d30-f0192d94e720",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}