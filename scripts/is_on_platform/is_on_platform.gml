
var numInstances = instance_number(o_collisionPlatform);
for (var iIndex = 0; iIndex < numInstances; iIndex++)
{
	var colEntity = instance_find(o_collisionPlatform, iIndex);
	if (colEntity.y >= bbox_bottom-0.01 and place_meeting(x, y+1, colEntity)) { return true; }
}
return false;
