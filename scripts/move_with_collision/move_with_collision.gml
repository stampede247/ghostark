
var pos_x = argument0[0];
var pos_y = argument0[1];
var vel_x = argument0[2];
var vel_y = argument0[3];
var stepHeight = argument1;
var isOnGround = argument2;
var collideWithPlatforms = argument3;
var velSign_x = sign(vel_x);

foundGroundToWalkOn = false;
if (isOnGround)
{
	if (abs(vel_x) > 0)
	{
		var offset_x = 0;
		var offset_y = 0;
		//Move up
		while (!check_collision_at(pos_x, pos_y + offset_y - 1, false, feetOffset) and offset_y > -stepHeight)
		{
			offset_y--;
		}
		//Move over
		while (!check_collision_at(pos_x + offset_x + 1*velSign_x, pos_y + offset_y, false, feetOffset) and abs(offset_x) < abs(vel_x))
		{
			offset_x += 1*velSign_x;
		}
		//Move down
		while (!check_collision_at(pos_x + offset_x, pos_y + offset_y + 1, collideWithPlatforms, feetOffset) and offset_y < stepHeight)
		{
			offset_y++;
		}
		if (check_collision_at(pos_x + offset_x, pos_y + offset_y+1, collideWithPlatforms, feetOffset))
		{
			foundGroundToWalkOn = true;
			pos_x += offset_x;
			pos_y += offset_y;
			vel_y = 0;
			if (abs(pos_x) < abs(vel_x)) { vel_x = 0; }
		}
	}
}

if (!foundGroundToWalkOn)
{
	//Horizontal collision and movement
	if (check_collision_at(pos_x + vel_x, pos_y, false, feetOffset))
	{
		var numPixelsMoved_x = 0;
		while(!check_collision_at(pos_x + ((numPixelsMoved_x+1) * sign(vel_x)), pos_y, false, feetOffset) and numPixelsMoved_x < abs(vel_x))
		{
			numPixelsMoved_x += 1;
		}
		pos_x += numPixelsMoved_x * sign(vel_x);
		vel_x = 0;
	}
	else { pos_x += vel_x; }

	//Vertical collision and movement
	if (check_collision_at(pos_x, pos_y + vel_y, (vel_y > 0 and collideWithPlatforms), feetOffset))
	{
		var numPixelsMoved_y = 0;
		while(!check_collision_at(pos_x, pos_y + ((numPixelsMoved_y+1) * sign(vel_y)), (vel_y > 0 and collideWithPlatforms), feetOffset) and numPixelsMoved_y < abs(vel_y))
		{
			numPixelsMoved_y += 1;
		}
		if (self.object_index == o_player and vel_y >= 1 and !isGrounded and !isGhost)
		{
			if (place_meeting(pos_x, pos_y + numPixelsMoved_y, o_mushroomArea))
			{
				audio_play_sound(snd_footstepMushroom2, 0, false);
			}
			else if (place_meeting(pos_x, pos_y + numPixelsMoved_y, o_woodArea))
			{
				audio_play_sound(snd_footstepWood2, 0, false);
			}
			else
			{
				audio_play_sound(snd_footstep2, 0, false);
			}
		}
		pos_y += numPixelsMoved_y * sign(vel_y);
		vel_y = 0;
	}
	else { pos_y += vel_y; }
}

argument0[@0] = pos_x;
argument0[@1] = pos_y;
argument0[@2] = vel_x;
argument0[@3] = vel_y;
