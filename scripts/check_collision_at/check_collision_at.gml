
var check_platforms = argument2;
var feetOffset = argument3;

if (place_meeting(argument0, argument1, o_collisionWall)) { return true; }
if (place_meeting(argument0, argument1, o_collisionIncline1)) { return true; }
if (place_meeting(argument0, argument1, o_collisionIncline2)) { return true; }
if (place_meeting(argument0, argument1, o_collisionSlope1)) { return true; }
if (self.object_index == o_player and !self.isGhost)
{
	if (place_meeting(argument0, argument1, o_collisionSpiritDoor)) { return true; }
	if (place_meeting(argument0, argument1, o_barrelWood)) { return true; }
}
if (self.object_index != o_player)
{
	if (place_meeting(argument0, argument1, o_barrelWood)) { return true; }
}
// if (check_platforms and place_meeting(argument0, argument1, platformObject)) { return true; }

if (true and check_platforms)
{
	var numInstances = instance_number(o_collisionPlatform);
	for (var iIndex = 0; iIndex < numInstances; iIndex++)
	{
		var colEntity = instance_find(o_collisionPlatform, iIndex);
		if (colEntity.y >= bbox_bottom-0.01 and place_meeting(argument0, argument1, colEntity)) { return true; }
	}
}



return false;