{
    "id": "88b86f4f-a735-40ef-a683-1ebaa4501d1d",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "debugFont",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Consolas",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "4815e741-afd6-4924-974b-c47307f6547a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "30b5399b-14c1-42a0-a5c3-dd47b88ec96e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 14,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 73,
                "y": 66
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "08b506aa-0279-4169-b795-8d974f279665",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 14,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 66,
                "y": 66
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "b3671923-f5bd-411a-b236-0d7ad24eae94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 57,
                "y": 66
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "11dd9be4-5441-422a-bc83-be522150c266",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 48,
                "y": 66
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "0694fa5c-cbee-4863-844e-e9ce18125145",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 39,
                "y": 66
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "471fdce2-5f3d-42f7-be66-a688f0aa969e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 30,
                "y": 66
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "44364cd7-0bd9-4b76-8708-b7eee9377977",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 14,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 25,
                "y": 66
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "22a126c5-85de-4719-b220-3f147d45890d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 14,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 18,
                "y": 66
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "7f1bf552-c061-429e-8ddc-9bd2dd6a56a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 14,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 11,
                "y": 66
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "697703fa-4014-41a4-ab04-c5fcfcf06325",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 78,
                "y": 66
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "d36160ab-a55f-41a2-b7c2-41e9a6d5e380",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 2,
                "y": 66
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "94a37a0e-ecb2-4e05-8f75-3fc8ba254573",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 14,
                "offset": 1,
                "shift": 7,
                "w": 4,
                "x": 107,
                "y": 50
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "9607ab14-214a-40a8-9501-bdd5937dd5d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 14,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 100,
                "y": 50
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "b7382fd5-69a3-48d6-9ea8-99e48a92a223",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 14,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 95,
                "y": 50
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "e2516a58-2ec3-4f65-a1de-fee43d0f700e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 87,
                "y": 50
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "d5bf882c-a792-467c-b191-ac7aa92c75ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 78,
                "y": 50
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "0b501d88-b4b7-4017-8ca4-8e5a42b3896f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 70,
                "y": 50
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "56942d23-b024-491e-bb3c-26bd85e4e04e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 62,
                "y": 50
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "e4718da6-7cad-4d35-ae3d-1fa4280b5913",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 54,
                "y": 50
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "496b19aa-aea6-48c3-80c4-ace656fc9ab1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 45,
                "y": 50
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "d4653b4a-b25e-4c05-9e59-a8c34beb5124",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 113,
                "y": 50
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "6eadd391-4bb3-45f6-8b8d-ec903834f923",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 86,
                "y": 66
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "d5f2d965-f40c-47a2-af2b-ada2882e68d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 95,
                "y": 66
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "89b378f3-85a2-4290-81ec-ebf9e7faf214",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 103,
                "y": 66
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "3bd93ab6-bf0c-4135-899a-485736b61cf4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 21,
                "y": 98
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "66edbc1a-1e5e-4150-80a8-766683b59c30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 14,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 16,
                "y": 98
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "04f30a4e-4e74-4324-ad4d-994b020c6ed6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 14,
                "offset": 1,
                "shift": 7,
                "w": 4,
                "x": 10,
                "y": 98
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "001549c2-06ff-43ad-be45-f5c1d903f09e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 2,
                "y": 98
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "0099246c-097a-43c0-b739-288fd29ba128",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 116,
                "y": 82
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "4476c5f0-1de1-4722-8fbc-c6a3be5f66bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 14,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 109,
                "y": 82
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "1516a0e4-da38-4fd0-8570-f330f247ee1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 14,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 102,
                "y": 82
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "21162e23-7fc8-4f3a-a624-4e8033dd9556",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 93,
                "y": 82
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "7c585629-7ed9-4078-a175-6aa02f5722e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 84,
                "y": 82
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "6ba8661a-7b39-4ee1-b8af-a7070c7b2922",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 75,
                "y": 82
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "107697ee-bd96-48b1-85aa-e12e0ef52748",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 67,
                "y": 82
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "c3846e3b-4952-40d6-a5f1-d047a64959f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 58,
                "y": 82
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "a9bc5cfb-1ede-40aa-a15e-d052f0ac705d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 14,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 51,
                "y": 82
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "18b449d6-1d2f-4834-8f69-91a23d46ebb3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 14,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 44,
                "y": 82
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "4c838fe2-9e93-471f-9602-34f2d8343a2b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 35,
                "y": 82
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "5b09d43b-bb08-4ec8-b41c-2f6c840dc50c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 26,
                "y": 82
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "74ef3c07-5ef0-4010-8a6c-f710e8f27ce7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 18,
                "y": 82
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "df9d3bcc-4820-4cc2-a878-197dac06e72b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 14,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 11,
                "y": 82
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "37e68bc8-1027-4b70-9054-70fcff37bdd2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 2,
                "y": 82
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "3c77e53c-839d-4eb1-8735-470bf0e6536a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 14,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 120,
                "y": 66
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "76b26def-d664-4e0a-9a5f-4567426c3aa6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 111,
                "y": 66
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "77c5a3f6-b83a-4b6e-a779-339340de714e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 36,
                "y": 50
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "fc2dafdd-6d2c-4387-8cb9-c97380b6a6f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 27,
                "y": 50
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "2088de25-c5d9-49ad-b81e-e9e7df22584c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 18,
                "y": 50
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "09d8d198-ece9-4001-8b80-47b7d2c37790",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 61,
                "y": 18
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "3d174d37-e8da-48b4-9fd1-57e42d281984",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 46,
                "y": 18
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "ad03eb1e-b885-455a-b099-2af5fad80fc3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 38,
                "y": 18
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "b677cee5-3b59-4770-b895-d9d912ce949e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 29,
                "y": 18
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "a4560211-83ad-4ca1-875f-8ebdf8b4d7a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 20,
                "y": 18
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "66fcbde1-5fb6-4033-b22c-0c15f3882b94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 11,
                "y": 18
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "837fd0da-0f20-48d9-9125-068c335f3371",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 2,
                "y": 18
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "e591137e-5ec6-42a4-ba43-c36d1d24f2d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 118,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "7b04f375-885b-4197-9675-671296cfd375",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 109,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "88b0aafd-f619-474d-8302-d2ec8cc799e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 101,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "629b22d0-8103-40a9-a890-f8ff88871921",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 14,
                "offset": 1,
                "shift": 7,
                "w": 4,
                "x": 55,
                "y": 18
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "252be602-802f-4b4a-b1e4-363a4ea61f20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 92,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "c5afe106-f7a1-4124-b5e7-36c89d757ace",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 14,
                "offset": 1,
                "shift": 7,
                "w": 4,
                "x": 77,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "5368bc48-131e-4b0e-917d-a17ee5b12778",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 68,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "76bc2531-1f54-464b-ba40-bfd9bbaf9600",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 59,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "f58bc12d-82aa-41e9-a8f3-1c00a9cf0ad5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 52,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "3a4cf1cd-d1a0-428d-865e-50cf4aab3ef1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 44,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "efcd5dcf-cfc8-40a2-b6c1-8a5c7764052a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 35,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "25717d19-7963-4e03-af22-9bf0d580873b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 27,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "cffab3a1-63ec-4679-95e5-44cef43142da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 19,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "84b14a2f-d891-4281-8304-30f295f702e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 11,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "ca4bbff0-bf81-4fd0-8691-dc487997e444",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 83,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "a49b3d5e-44df-4620-9dc8-899287dd2aaa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 70,
                "y": 18
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "54e15143-aca6-4d4c-baec-f9967a181dd7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 37,
                "y": 34
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "21dda388-a807-48b5-81e7-014ec683f123",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 79,
                "y": 18
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "eb7fd21f-6e6f-409c-9df2-3475e7ff4f3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 2,
                "y": 50
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "ee7fd5d6-8948-435b-b9d7-4c8899393845",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 112,
                "y": 34
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "070d1318-cb98-44a3-8f6a-2b70b3a06f67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 104,
                "y": 34
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "c5e9a5ea-47ed-4e9b-8184-8d3be5b450ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 95,
                "y": 34
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "445c8292-c36b-4fae-80f2-3423334fa067",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 87,
                "y": 34
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "b327c101-e299-4618-b763-28051295cedd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 78,
                "y": 34
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "fc21357f-3877-46cf-b8b7-00409a42197e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 69,
                "y": 34
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "1bd84141-1cab-4b0d-8d7e-2159b25c64c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 61,
                "y": 34
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "8e48fb22-03f3-40a2-8f0b-587764c7a754",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 14,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 53,
                "y": 34
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "fa93e457-4f78-48ea-8835-de107ffecb69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 10,
                "y": 50
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "06da6798-4fbb-4e00-9fa9-87e5304cf471",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 45,
                "y": 34
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "0ddd7211-9b1c-4158-98ad-24145185b0af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 29,
                "y": 34
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "082b6877-40e5-49a5-b9e7-df692b4b17f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 20,
                "y": 34
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "ea924e68-28dd-46d8-a095-a9d23841c10e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 11,
                "y": 34
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "1dd48fa5-5eab-42b5-9b46-11bef53b21f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 2,
                "y": 34
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "cd5a03bc-06ef-45d4-914a-31cfa6bc0460",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 114,
                "y": 18
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "6d2bf93f-dc39-4711-acbf-3db274101670",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 106,
                "y": 18
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "3301d7b9-f36d-49db-a1ee-e60f62a7fe25",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 98,
                "y": 18
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "377f07eb-fcc9-44d4-b30a-eecce52fdc62",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 14,
                "offset": 2,
                "shift": 7,
                "w": 2,
                "x": 94,
                "y": 18
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "6ca9c437-12ca-415d-a7a0-f6ab25a1dba8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 14,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 87,
                "y": 18
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "e8fc0c9c-433a-4a4a-b054-367ea3f0019a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 29,
                "y": 98
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "0c2b3b74-8ac4-461e-b738-588be7b86116",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 14,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 38,
                "y": 98
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 9,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}