{
    "id": "7dec70a6-b40e-4786-814e-5773b8625ecf",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "endFont",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Courier New",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "978c187f-59c0-4b66-b87e-e7d38f8aacc6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 30,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "0a5aa1f9-470c-49f6-b593-cd4ed9fca51b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 30,
                "offset": 5,
                "shift": 16,
                "w": 6,
                "x": 74,
                "y": 130
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "e183e536-1c3a-47d4-bf58-a49f9c85ec34",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 30,
                "offset": 3,
                "shift": 16,
                "w": 10,
                "x": 62,
                "y": 130
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "c7df6694-8125-422f-9d7b-6776d4c369f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 30,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 46,
                "y": 130
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "2e116e50-97f7-49ea-9d57-f9685b475cd1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 30,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 32,
                "y": 130
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "de983424-31c3-434d-aa36-f7a414dddc6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 30,
                "offset": 2,
                "shift": 16,
                "w": 13,
                "x": 17,
                "y": 130
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "c2e49274-0579-47e2-8152-a0c4d380db32",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 30,
                "offset": 2,
                "shift": 16,
                "w": 13,
                "x": 2,
                "y": 130
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "2c1ab031-1839-4563-8ed5-dcb031cac905",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 30,
                "offset": 6,
                "shift": 16,
                "w": 4,
                "x": 241,
                "y": 98
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "2c7174b0-071b-4c49-8613-2de3acd7bb07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 30,
                "offset": 7,
                "shift": 16,
                "w": 7,
                "x": 232,
                "y": 98
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "eee329e5-2196-490f-b8f4-aaa86be23bbf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 30,
                "offset": 3,
                "shift": 16,
                "w": 7,
                "x": 223,
                "y": 98
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "fcc63b51-9e3c-4e01-af47-55dc92f2c628",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 30,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 82,
                "y": 130
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "ed3e429d-7b6e-409e-a378-8735349148c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 30,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 206,
                "y": 98
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "6a22d588-0d1e-4acb-94bd-6bd44ed8396f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 30,
                "offset": 5,
                "shift": 16,
                "w": 7,
                "x": 181,
                "y": 98
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "74027b01-f912-45ef-991e-07f25f9a7260",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 30,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 167,
                "y": 98
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "18c88545-c1c0-4443-90e4-2efcac8fbe5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 30,
                "offset": 6,
                "shift": 16,
                "w": 5,
                "x": 160,
                "y": 98
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "ee9e335b-483f-48ea-b269-e25b0e11cae5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 30,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 146,
                "y": 98
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "7e58e3b7-eece-4fa8-9088-1dd488568fe4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 30,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 132,
                "y": 98
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "868e6394-fc85-43b5-b904-ee04ad24aab7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 30,
                "offset": 2,
                "shift": 16,
                "w": 13,
                "x": 117,
                "y": 98
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "91e77a4f-71ac-47b8-838c-66ce72f5ea60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 30,
                "offset": 1,
                "shift": 16,
                "w": 13,
                "x": 102,
                "y": 98
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "233bb5d9-7e92-46c8-9059-6c449d1ca6d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 30,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 86,
                "y": 98
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "9687ee6e-6cc3-4209-85e1-e3138f4c081a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 30,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 72,
                "y": 98
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "5898778a-a998-4594-98b2-1c66184a5ca6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 30,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 190,
                "y": 98
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "9f11fa72-a5de-422f-9836-58b24b2a47de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 30,
                "offset": 2,
                "shift": 16,
                "w": 13,
                "x": 96,
                "y": 130
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "8855cda3-5564-4de2-b848-3b1361f167fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 30,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 111,
                "y": 130
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "b437044c-3d32-4f02-aca3-fb9cd75d5ac4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 30,
                "offset": 2,
                "shift": 16,
                "w": 13,
                "x": 125,
                "y": 130
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "b2ea2399-483c-4b0d-9af8-1f350e91ad35",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 30,
                "offset": 2,
                "shift": 16,
                "w": 13,
                "x": 227,
                "y": 162
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "c2d10e3b-4ad5-4ce4-b250-e73f5c3f83dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 30,
                "offset": 6,
                "shift": 16,
                "w": 5,
                "x": 220,
                "y": 162
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "53ddbb8f-291e-4375-b707-a59ed7ba63cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 30,
                "offset": 5,
                "shift": 16,
                "w": 7,
                "x": 211,
                "y": 162
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "ca89c51b-4bb3-45c9-b3c4-0776bfc0dfa7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 30,
                "offset": -1,
                "shift": 16,
                "w": 17,
                "x": 192,
                "y": 162
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "b4bafd36-62ae-4dd8-8a23-d78b4e962cae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 30,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 174,
                "y": 162
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "d1d341c3-4e23-45ef-ac9c-df6560d21430",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 30,
                "offset": 1,
                "shift": 16,
                "w": 16,
                "x": 156,
                "y": 162
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "a469177d-cc68-4cec-9162-7d3ec67cf487",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 30,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 142,
                "y": 162
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "6455da48-c96f-45cc-bce7-bb41e32f43ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 30,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 128,
                "y": 162
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "a087ae92-267c-48d9-809f-1723e2a9dad3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 30,
                "offset": -1,
                "shift": 16,
                "w": 18,
                "x": 108,
                "y": 162
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "e170f889-6e24-475d-bcf5-8ce3c0b24c5b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 30,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 90,
                "y": 162
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "795cd7e7-4efa-4fa4-a9ad-186fce60dcec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 30,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 72,
                "y": 162
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "a7b1ae7e-69f3-4f88-9ede-6541e375de2d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 30,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 55,
                "y": 162
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "7d1fc7de-be12-4931-b1df-f3f00a225e9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 30,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 38,
                "y": 162
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "3374c5f1-7827-46bb-b210-ae524550b1d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 30,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 21,
                "y": 162
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "b5477a4b-c84c-48dc-9812-a2ef0bcf5302",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 30,
                "offset": 0,
                "shift": 16,
                "w": 17,
                "x": 2,
                "y": 162
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "c47054ad-06bf-44fd-b64c-38b1dc2027eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 30,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 229,
                "y": 130
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "f2af6e7c-b7f2-4f7c-92e6-1133bb16f248",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 30,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 215,
                "y": 130
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "c2dd5815-7371-43ff-b220-a7b55d6fc325",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 30,
                "offset": 1,
                "shift": 16,
                "w": 16,
                "x": 197,
                "y": 130
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "41c2d1ce-9b74-4ef9-8ae8-e9b38e05a82b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 30,
                "offset": 0,
                "shift": 16,
                "w": 17,
                "x": 178,
                "y": 130
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "289031a3-94d7-4b7f-b0b0-976a60fef7e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 30,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 160,
                "y": 130
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "e4242c89-65a5-4810-a98c-3d62c8e3500f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 30,
                "offset": -1,
                "shift": 16,
                "w": 18,
                "x": 140,
                "y": 130
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "1cc12d04-095c-4d1e-aff4-b2e29fa69425",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 30,
                "offset": -1,
                "shift": 16,
                "w": 17,
                "x": 53,
                "y": 98
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "a5c6330e-6263-40e9-bd2a-29962ba00e29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 30,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 35,
                "y": 98
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "6acf1c7c-3da1-48de-b58f-bd4b9ae7f8de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 30,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 18,
                "y": 98
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "b06e250f-9b4b-47f3-bbc5-4d4678b6e0d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 30,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 121,
                "y": 34
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "00f7b59e-c541-4e83-b56c-0734da3eb735",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 30,
                "offset": 0,
                "shift": 16,
                "w": 17,
                "x": 93,
                "y": 34
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "b23606e7-5735-4363-8cff-88217c2374d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 30,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 77,
                "y": 34
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "1106ebe2-faa3-4bf4-99f3-c6f4ef246369",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 30,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 60,
                "y": 34
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "a0b5fbc2-0e23-4f1d-949b-b567451f40ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 30,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 42,
                "y": 34
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "9f2ea670-06b4-479c-bb4f-ecaa246bbf69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 30,
                "offset": -1,
                "shift": 16,
                "w": 18,
                "x": 22,
                "y": 34
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "2ba78908-ad16-42e4-bb46-528d631d467d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 30,
                "offset": -1,
                "shift": 16,
                "w": 18,
                "x": 2,
                "y": 34
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "93a520c4-1699-465c-a989-da669a757565",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 30,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 227,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "32e6b43a-f9d1-4a3c-9870-13cdf1cdfd98",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 30,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 209,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "66ba3a0f-01db-4179-9200-614235ebc625",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 30,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 193,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "456d4ab2-845f-439c-b43c-178c7c093d9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 30,
                "offset": 6,
                "shift": 16,
                "w": 7,
                "x": 112,
                "y": 34
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "d2f72d14-ff73-4565-9883-faba4415d7d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 30,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 179,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "4a63a4b5-2c92-4396-8cc7-944532fdd738",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 30,
                "offset": 3,
                "shift": 16,
                "w": 7,
                "x": 153,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "67b05081-1bd6-45d9-ab3d-d412869d27ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 30,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 139,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "2beb12ff-f15d-4b6f-9647-19d99bf54560",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 30,
                "offset": -2,
                "shift": 16,
                "w": 20,
                "x": 117,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "5f15d1f4-e5c9-4ea2-9d91-9d25a20ec45a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 30,
                "offset": 5,
                "shift": 16,
                "w": 6,
                "x": 109,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "08b9f9bb-e742-4751-ad60-5ca6dee18b7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 30,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 92,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "b9997e40-fe5f-44a0-9e06-656b43d4c27d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 30,
                "offset": -1,
                "shift": 16,
                "w": 17,
                "x": 73,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "d97809fd-ec62-4994-a19c-2b87a0ff2834",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 30,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 56,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "c5484554-725c-4ceb-91ec-27e0d126e1a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 30,
                "offset": 0,
                "shift": 16,
                "w": 17,
                "x": 37,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "209ea607-4757-4eed-93f4-ec89107fd1bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 30,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 20,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "fef76981-e799-4242-b4d3-ae4de3f27f4c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 30,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 162,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "5ec56969-17a1-4240-8ad3-eb106a5ab6b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 30,
                "offset": 0,
                "shift": 16,
                "w": 17,
                "x": 139,
                "y": 34
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "6dc189f4-4845-48fd-a4c5-509cbb9fd30a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 30,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 59,
                "y": 66
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "c660ff9c-ef96-4f54-845d-ab1763da4b2c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 30,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 158,
                "y": 34
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "caa28483-06ad-4898-a199-dcb2576221d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 30,
                "offset": 2,
                "shift": 16,
                "w": 11,
                "x": 238,
                "y": 66
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "7b88b83d-f3db-400b-85d3-0cd5131ee930",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 30,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 220,
                "y": 66
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "cd706276-ea2e-4a6e-bf1b-daea32b35aa7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 30,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 204,
                "y": 66
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "ecee4b7f-9335-4ed4-a945-b91f4be64c69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 30,
                "offset": -1,
                "shift": 16,
                "w": 18,
                "x": 184,
                "y": 66
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "e23f9265-e0d2-4802-92ae-fb1f2e612cd1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 30,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 166,
                "y": 66
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "33c55baf-0139-4b94-bb4d-409b843cde87",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 30,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 149,
                "y": 66
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "d0194bc0-3e7c-4f9f-a1e3-e7176628cada",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 30,
                "offset": -1,
                "shift": 16,
                "w": 17,
                "x": 130,
                "y": 66
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "c56cf454-cb66-44b6-8f8c-f494103bc03e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 30,
                "offset": 0,
                "shift": 16,
                "w": 17,
                "x": 111,
                "y": 66
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "fd2d486c-3aa7-417b-aea6-d3102fb30469",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 30,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 94,
                "y": 66
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "92e606dd-dc9d-4265-91a1-fd3daa54e8e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 30,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 2,
                "y": 98
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "99be73ce-4797-4063-8aab-e339f3d951fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 30,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 77,
                "y": 66
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "b191d91f-f7da-4810-af95-7fcefaecf909",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 30,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 41,
                "y": 66
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "f28411e2-33fd-4865-9e0e-da5edfca3ab5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 30,
                "offset": 0,
                "shift": 16,
                "w": 17,
                "x": 22,
                "y": 66
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "91b5b33b-bf89-48c7-a90f-b9779b91a443",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 30,
                "offset": -1,
                "shift": 16,
                "w": 18,
                "x": 2,
                "y": 66
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "14846b2a-cf9c-40f2-a80b-a0d1ce8c2d45",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 30,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 233,
                "y": 34
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "576ee9a7-dca8-43aa-8b9d-42780f5bcd34",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 30,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 215,
                "y": 34
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "20144340-9b01-4899-89cd-07cee3e45f91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 30,
                "offset": 2,
                "shift": 16,
                "w": 13,
                "x": 200,
                "y": 34
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "e067d406-c7db-4818-944b-914e4de7151d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 30,
                "offset": 4,
                "shift": 16,
                "w": 8,
                "x": 190,
                "y": 34
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "c6145829-1b3a-468b-9e67-d3d3b7467623",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 30,
                "offset": 6,
                "shift": 16,
                "w": 4,
                "x": 184,
                "y": 34
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "89ac8666-3b9c-42c8-84d0-4e66dc783a4d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 30,
                "offset": 4,
                "shift": 16,
                "w": 8,
                "x": 174,
                "y": 34
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "f5351931-67b9-405f-9d98-53c20b6065f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 30,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 2,
                "y": 194
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "9b7df4e6-901b-429f-9369-59588b53e06d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 30,
                "offset": 5,
                "shift": 16,
                "w": 7,
                "x": 18,
                "y": 194
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 20,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}